// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const ApiUrls = {
    taskVisu: {
        task: 'http://schertech.com:84/task_visu/php_new/task_visu_data_service.php?service=',
        task_new: 'http://schertech.com:84/task_visu/php_new/task_service.php?service=',
        note: 'http://schertech.com:84/task_visu/php_new/task_visu_data_service.php?service=',
        time: 'http://schertech.com:84/task_visu/php_new/task_time_data_service.php?service=',
        config: 'http://schertech.com:84/task_visu/php_new/config_data_service.php?service=',
        team: 'http://schertech.com:84/task_visu/php_new/team_data_service.php?service=',
        project: 'http://schertech.com:84/task_visu/php_new/project_service.php?service=',
        gantt: 'http://schertech.com:84/task_visu/php_new/gantt_service.php?service='
    },
    baseVisu: {
        ip: 'http://schertech.com:84/base_visu/',
        sessionUrl: 'http://schertech.com:84/user_session/php/check_session.php?module_name=taskvisu&client_id=8',
        sessionData: 'http://schertech.com:84/user_session/php/session_data_service.php?service=get_session',
        userList: 'http://schertech.com:84/base_visu/php/user_permission_data_service.php?service=get_Specific_Module_User&module_name=taskvisu'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
