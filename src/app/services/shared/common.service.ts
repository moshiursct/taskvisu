import { Injectable, EventEmitter } from '@angular/core';
import { Note } from 'src/app/models/note.model';
import { Task } from './../../models/task.model';
import { User } from './../../models/user.model';
import { getCurrentTime } from '../../helpers/helper'
import { CommonHttpService } from "../http/common.service";
import { Module } from 'src/app/models/module.model';
import { TaskType } from 'src/app/models/task-type.model';
import { Team } from 'src/app/models/team.model';
import { Sprint } from 'src/app/models/sprint.model';
import { Category } from 'src/app/models/category.model';

@Injectable({
	providedIn: 'root'
})
export class CommonService {
	taskRowClicked = new EventEmitter();
	sidebarCommonFilter = new EventEmitter();
	noteTabClicked = new EventEmitter();
	refreshTaskGrid = new EventEmitter();

	currentTaskNoteList: Note[] = [];
	taskof: number;
	oldTaskId: number = 0;
	userList: Array<User> = [];
	currentTask: Task;
	currentTimeDot: string;
	currentTimeHyphen: string;
	
	public currentUser: { id: number, name: string } = {
		id: 101006,
		name: 'Tanbir Mahin'
	};
	
	public teamList: Team[] = [];
	public sprintList: Sprint[] = [];
	public projectList: Array<any> = [];
	public categoryList: Category[] = [];
	public taskTypeList: Array<any> = [];
	public isTaskDataChanged: boolean = false;

	public taskStatusList = []
	public taskTimeData = null;

	clientList: {}[] = [];
	moduleList: Array<Module> = [];

	route: string;

	constructor() {
		this.currentTimeDot = getCurrentTime('.');
		this.currentTimeHyphen = getCurrentTime('-');
	}

	onTaskRowClick(rowData: any): void {
		this.taskRowClicked.emit(rowData);
	}

	onSidebarCommonFilter(sidebarData: any): void {
		this.sidebarCommonFilter.emit(sidebarData);
	}

	weekStartEndDate(type: String): String {
		// let firstday;
		// let lastday;
		// if (type == "this_week") {
		//     firstday = moment().startOf("week").toDate();
		//     lastday = moment().endOf("week").toDate();
		// } else {
		//     firstday = moment().add(1, 'weeks').startOf("week").toDate();
		//     lastday = moment().add(1, 'weeks').endOf("week").toDate();
		// }

		// firstday = firstday.getDate() + "." + (firstday.getMonth() + 1) + "." + firstday.getFullYear() + " 00:00";
		// lastday = lastday.getDate() + "." + (lastday.getMonth() + 1) + "." + lastday.getFullYear() + " 00:00";
		// var fisrt_last_date = { first: firstday, last: lastday };
		// return fisrt_last_date;
		return '';
	}

	onTaskGridRefesh(): void {
		this.refreshTaskGrid.emit();
	}
}
