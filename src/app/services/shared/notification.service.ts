import { Injectable } from '@angular/core';
import { IndividualConfig, ToastrService } from 'ngx-toastr';

@Injectable({
  	providedIn: 'root'
})
export class NotificationService {

	options: IndividualConfig;

  	constructor(private toastr: ToastrService) {
		this.options = this.toastr.toastrConfig;
		this.options.enableHtml = true;
		this.options.closeButton = true;
        this.options.tapToDismiss = true;
        this.options.timeOut = 3000;
        this.options.progressBar = true;
        this.options.progressAnimation = 'decreasing';
        this.options.positionClass = 'toast-bottom-right';
        this.options.messageClass = 'toast-message';
        this.options.newestOnTop = true;
	}

	showSuccessMsg(msg: string) {
    	this.toastr.success(msg);
	}
	  
	showErrorMsg(msg: string) {
    	this.toastr.error(msg);
  	}
}
