import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ApiUrls } from './../../../environments/environment';
import { User } from './../../models/user.model';
import { TaskType } from './../../models/task-type.model';
import { Sprint } from './../../models/sprint.model';
import { Team } from './../../models/team.model';
import { TaskStatus } from '../../models/task-status.model';

@Injectable({
	providedIn: 'root'
})
export class CommonHttpService {
	// private BASE_VISU_IP: String = ApiUrls.baseVisu.ip;

	constructor(private http: HttpClient) { }

	getPersonList(): Observable<User[]> {
		return this.http.get(ApiUrls.baseVisu.userList).pipe(
			map(response => (<User[]>response['records'].map(user => {
				return {
					id: user.mitarbeiter_nr,
					name: user.mitarbeiterbez,
					email: user.email,
					userCode: user.user_code,
					userType: user.user_type
				}
			})))
		);
	}

	getOptionsList() {
		return this.http.get(`${ApiUrls.taskVisu.config}get_options`);
	}

	getTaskType(): Observable<TaskType[]> {
		return this.http.get<{ records: Array<TaskType> }>(`${ApiUrls.taskVisu.config}get_task_types&take=all&skip=0`).pipe(
			map(response => (response['records'].map(taskType => {
				return taskType;
			})))
		);
	}

	getSprints(): Observable<Sprint[]> {
		return this.http.get<{ records: Array<Sprint> }>(`${ApiUrls.taskVisu.team}get_sprint_data&take=All&skip=0`).pipe(
			map(response => (response['records'].map(sprints => {
				return sprints;
			})))
		);
	}

	getTeams(): Observable<Array<Team>> {
		return this.http.get<{ records: Array<Team> }>(`${ApiUrls.taskVisu.team}get_teams_list&userid=406`).pipe(
			map(response => (response['records'].map((teams: any) => {
				let memberList = teams.memberList.split(',')
				return new Team(teams.id, teams.name, memberList, teams.teamDetails, teams.isArchived, teams.creatorNr);
			})))
		);
	}

	deleteAttachments(files: File[]): Observable<any> {
		return this.http.post(`${ApiUrls.taskVisu.task_new}delete_attachments`, files);
	}

	getStatusList(): Observable<Array<TaskStatus>> {
		return this.http.get<{ records: Array<TaskStatus> }>(`${ApiUrls.taskVisu.time}get_status_list_data&take=all&skip=0`).pipe(
			map(response => (response['records'].map((status: any) => {
				return new TaskStatus(status.id, status.status, status.color, status.in_use, status.type, status.updated_time);
			})))
		);
	}

	getUserTimerData(): Observable<any> {
		return this.http.get(`${ApiUrls.taskVisu.time}get_user_status&user_id=` + '101006')
	}
}
