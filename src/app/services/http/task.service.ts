import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from "rxjs";
import { map, tap } from 'rxjs/operators';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { ApiUrls } from './../../../environments/environment';
import { Task } from '../../models/task.model';

@Injectable({
	providedIn: 'root'
})
export abstract class GridService extends BehaviorSubject<GridDataResult> {
	public loading: boolean;
	private BASE_URL = `${ApiUrls.taskVisu.task_new}`;
	private Task_BASE_URL = `${ApiUrls.taskVisu.task_new}`;
	private TIME_BASE_URL = `${ApiUrls.taskVisu.time}`;

	constructor(
		private http: HttpClient) {
		super(null);
	}

	query(state: any, taskQueryParam: any): void {
		this.fetch(state, taskQueryParam).subscribe(x => super.next(x));
	}

	protected fetch(state: any, taskQueryParam: any): Observable<GridDataResult> {
		let filter_sort = '';

		if (state.filter?.filters.length) {
			filter_sort += `&filters=${JSON.stringify(state.filter.filters)}`;
		}

		if (state.sort?.length) {
			filter_sort += `&sort=${JSON.stringify(state.sort)}`;
		}
		const queryString = `${this.BASE_URL}get&user_id=${taskQueryParam.userId}&start_date=${taskQueryParam.startDate}&end_date=${taskQueryParam.endDate}&status=${taskQueryParam.status}&type=${taskQueryParam.type}&parent_id=${taskQueryParam.parentId}&sub_flag=${taskQueryParam.subFlag}&skip=${state.skip}&take=${state.skip + state.take}${filter_sort}`;
		console.log(queryString);
		this.loading = true;
		return this.http
			.get(queryString)
			.pipe(
				map(response => (<GridDataResult>{
					data: response['records'],
					total: response['total']
				})),
				tap(() => this.loading = false)
			);
	}

	taskDetails(taskId: any): Observable<Task> {
		const queryString = `${this.BASE_URL}get_details&task_id=${taskId}`;
		return this.http.get<Task>(queryString);
	}

	delete(id: number): Observable<any> {
		// const headers = new HttpHeaders({ 'Content-Type' : 'application/json'});
		const queryString = `${this.Task_BASE_URL}delete`;
		return this.http.post<Task>(queryString, id);
	}

	getTimeSheetData(taskId, currentTime) {
		return this.http.get(`${this.TIME_BASE_URL}get_task_time_list&task_id=${taskId}&current_time=${currentTime}`);
	}

	public save(task: Task): Observable<any> {
		console.log(task);
		return this.http.post(`${this.Task_BASE_URL}save`, task);
	}

}
@Injectable({
	providedIn: 'root'
})
export class TaskService extends GridService {

	public taskData: Task;
	public uploader;
	fileCount: number = 0;
	constructor(http: HttpClient) {
		super(http);
	}
	public upload() {
		this.uploader.uploadFiles();
		console.log(this.fileCount);

	}
	public completeEventHandler() {
		console.log(`All files upload completed`);
		this.saveTaskData();
		this.fileCount = 0;
	}
	public saveTaskData(): void {
		console.log("task saved", this.taskData);
	}

	// queryAll(st?: any): Observable<GridDataResult> {
	//     const state = Object.assign({}, st);
	//     delete state.take;
	//     delete state.skip;
	//     return this.fetch(state, {});
	// }

}
