import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiUrls } from '../../../environments/environment';

@Injectable({
	providedIn: 'root'
})
export class GanttService {
	private URL: string = ApiUrls.taskVisu.gantt;

	public phaseList: any = [];
	public currentPhase: any = "";

	public ganttClicked: EventEmitter<any> = new EventEmitter<any>();
	public dataChanged: EventEmitter<any> = new EventEmitter<any>();

	constructor(private http: HttpClient) {}

	getList(projectId: any, userId: any): Observable<any> {
		const url = `${this.URL}getAll&project_id=${projectId}&user_id=${userId}`;

		return this.http.get(url);
	}

	save(data: any, projectId: any): Observable<any> {
		const url = `${this.URL}save&project_id=${projectId}`;

		return this.http.post(url, data);
	}

	onGanttClicked(projectData: any): void {
		this.ganttClicked.emit(projectData);
	}

	onDataChanged(data: any): void {
		this.dataChanged.emit(data);
	}
}
