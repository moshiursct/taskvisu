import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { Note } from './../../models/note.model';
import { ApiUrls } from './../../../environments/environment';

@Injectable({
    providedIn: 'root'
})

export class NoteService {
    private noteURL: string = ApiUrls.taskVisu.note;

    constructor(private http: HttpClient) { }

    getList(taskId: number): Observable<Note[]> {
        const queryString = `${this.noteURL}get_task_notes&task_id=${taskId}`;
        return this.http.get<Note[]>(queryString);
    }

    save(data: Note): Observable<Note> {
        const queryString = `${this.noteURL}save_task_note`;
        return this.http.post<Note>(queryString, data);
    }
}