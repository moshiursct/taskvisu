import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiUrls } from '../../../environments/environment';

@Injectable({
	providedIn: 'root'
})
export class ProjectService {
	private URL: string = ApiUrls.taskVisu.project;

	public projectClicked: EventEmitter<any> = new EventEmitter<any>();
	public dataChanged: EventEmitter<any> = new EventEmitter<any>();
	public statusList: Array<any>;

	constructor(private http: HttpClient) {
		this.statusList = [
			{
				id: 1,
				status: "offen",
				name: "Ongoing Project(s)" 
			},
			{
				id: 2,
				status: "closed",
				name: "Completed Project(s)" 
			},
			{
				id: 3,
				status: "alle",
				name: "All Projects" 
			}
		];
	}

	getList(userId, status): Observable<any> {
		const url = `${this.URL}getAll&status=${status}&user_id=${userId}`;

		return this.http.get(url);
	}

	save(data: any): Observable<any> {
		const url = `${this.URL}save`;

		return this.http.post(url, data);
	}

	onProjectClicked(projectData: any): void {
		this.projectClicked.emit(projectData);
	}

	onDataChanged(data: any): void {
		this.dataChanged.emit(data);
	}
}
