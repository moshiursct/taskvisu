import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';
import { ApiUrls } from "../../../environments/environment";
import { DevStatus } from "../../models/dev-status.model";

@Injectable({
  providedIn: 'root'
})
export class DevStatusService {

  constructor(private http: HttpClient) { }

  private devStatusUrl:string = ApiUrls.taskVisu.task;

  getDevStatusList(taskId: number): Observable<{total: number, records: DevStatus[]}> {
      
    return this.http.get<{total: number, records: DevStatus[]}>(`${this.devStatusUrl}get_task_dev_status_list&task_id=${taskId}`);
  }

  saveDevStatusData(devStatusData): Observable<number>{
    return this.http.post<number>(`${this.devStatusUrl}save_task_dev_status`, devStatusData);
  }

  deleteDevStatusData(devId): Observable<number> {
    return this.http.post<number>(`${this.devStatusUrl}remove_task_requirement`, devId);
  }

}
