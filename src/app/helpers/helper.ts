export function getCurrentTime(type: string): string {
    const today = new Date();
    if (type == '.') {
        const date = today.getDate() + '.' + (today.getMonth() + 1) + '.' + today.getFullYear();
        const time = today.getHours() + ":" + today.getMinutes();
        return date + ' ' + time;
    }
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    return date + ' ' + time;
}

export function objectEquals(f: Object, s: Object): boolean {
    if(!isObject(f) || !isObject(s)) return false;
    const k1 = Object.keys(f);
    const k2 = Object.keys(s);

    if (k1.length !== k2.length) return false;

    for (const k of k1) {
        const v1 = f[k];
        const v2 = s[k];
        if (Array.isArray(v1) && Array.isArray(v2)) {
            if (v1.length != v2.length) return false;
            const ae = arrayCompare(v1, v2);
            if (!ae) return ae;
        } else if(v1 instanceof Date) {
            if(v2 instanceof Date) { if( v1.getTime() != v2.getTime() ) return false;}
            else return false;
        } {
            const areObj = isObject(v1) && isObject(v2);
            if (areObj && !objectEquals(v1, v2) || !areObj && v1 !== v2) return false;
        }
    }
    return true;
}

const isObject = (obj: any) => obj != null && typeof obj === 'object';

const arrayCompare = (a1: Array<any>, a2: Array<any>) => {
    if (typeof a1[0] === 'object') {
        const k = Object.keys(a1[0]);
        a1 = a1.sort((a, b) => a[k[0]] - b[k[0]]);
        a2 = a2.sort((a, b) => a[k[0]] - b[k[0]]);
    } else {
        a1 = a1.sort((a, b) => a - b);
        a2 = a2.sort((a, b) => a - b)
    }
    let e = true;
    for (let x = 0; x < a1.length; x++) {
        if (typeof a1[x] === 'object') {
            e = objectEquals(a1[x], a2[x]);
            if (!e) return e;
        } else {
            if (a1[x] != a2[x]) return false;
        }
    }
    return e;
}

