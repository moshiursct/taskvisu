export interface File {
    "id"?: number,
    "name": string,
    "uid"?: string,
    "modifiedName"?: string,
    "extension": string,
    "path": string,
    "size": string
}