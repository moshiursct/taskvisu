import { ComponentPermission } from "./component-permission.model";

export class TaskType {
   constructor(
      public id: number,
      public typeName: string,
      public isDeletable: boolean,
      public componentPermission: ComponentPermission[]
   ) {

   }
}