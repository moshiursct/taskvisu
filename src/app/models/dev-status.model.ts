export interface DevStatus {
    "id": number,
    "taskId": number,
    "requirement": string,
    "details": string,
    "designedBy": [],
    "designProgress": number,
    "developedBy": [],
    "developmentProgress": number,
    "initialTestBy": [],
    "initialTestProgress": number,
    "feedbackBy": [],
    "feedbackProgress": number,
    "releasedBy": number,
    "releaseProgress": number,
    "implementedIn": [],
    "understanding": number,
    "implementationProgress": {}
}