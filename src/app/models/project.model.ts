import { File } from '../models/file.model';

export interface Project {
    "id"?: number,
    "name": string,
    "progress": number,
    "personNr": string,
    "partsNr": string,
    "participantsNr": string,
    "date": Date,
    "details": string,
    "isArchived"?: boolean,
    "attachments": File[]
}