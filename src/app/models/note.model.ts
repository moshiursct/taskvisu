export interface Note {
    "id": number,
    "task_id": number,
    "user_id": string,
    "note": string,
    "created_at"?: string,
    "updated_at"?: string
}