export class ComponentPermission {


    constructor(
        public id: number,
        public componentId: number,
        public isChecked: boolean,
        public isDisabled: boolean,
        public isMandatory: boolean,
        public multiLangKey: string,
        public name: string,
        public model: string,
        public typeId: number
        ) {

    }
}

