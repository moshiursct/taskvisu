export interface Task {
	"id": number,
	"typeId": number,
	"typeName"?: string,
	"progress": number,
	"title": string,
	"details": string,
	"personNr": number,
	"contactPersons": [],
	"responsibleNr": number,
	"subResponsibles": [],
	"startDate": Date,
	"endDate": Date,
	"closedDate": Date,
	"estTime": Date,
	"consumedTime"?: Date,
	"parentName": string,
	"clientId": number,
	"sprintId": number,
	"moduleId": number,
	"categoryId": number,
	"priorityColor": string,
	"teamId": number,
	"regelMeetingId": number,
	"projectId": number,
	"meetingId": number,
	"attachments": any,
	"consumendTime": string,
	"partsNr": [], //teile_nr
	"machineNr": [], // maschine_nr
	"clientNr": [], // kunden_nr
	"toolsNr": [], // werkuge/wkz_nr
	"supplierNr": [], // lieferant_nr
	"isRead": boolean,
	"isArchived": boolean,
	"createdAt"?: string,
	"updatedAt"?: string,
	"createdBy"?: number,
	"updatedBy": number
}
