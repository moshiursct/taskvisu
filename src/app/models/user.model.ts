export interface User {
    "id": string,
    "name": string,
    "email": string,
    "userCode": string,
    "userType": string
}