export class Sprint {
    constructor(
        private id: number,
        private name: string,
        private startDate: string,
        private endDate: string,
        private used: number
    ) {}
}