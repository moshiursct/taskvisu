export class TaskStatus {
    public id: number;
    public status: string;
    public color: string;
    public inUse: string;
    public type: string
    public updatedTime: string


    constructor(id: number, status: string, color: string, in_use: string, type: string, updated_time:string) {
        this.id = id;
        this.status = status;
        this.color = color;
        this.inUse = in_use;
        this.type = type;
        this.updatedTime = updated_time 
    }
}