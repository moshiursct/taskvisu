export class Team {
    constructor(
        public id:number,
        public name: string,
        public memberList: Array<number>,
        public teamDetails:string, 
        public isArchived: boolean,
        public creatorNr: number
    ){}
}