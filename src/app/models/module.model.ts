export class Module {
    public id: number;
    public name: string;
    public type: string;
    public used?: number;
    public createdAt: string
    public updatedAt: string


    constructor(id: number, name: string, type: string, createdAt: string, updatedAt: string) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }
}