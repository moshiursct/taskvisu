import { Pipe, PipeTransform } from '@angular/core';
import { CommonService } from './../services/shared/common.service';

@Pipe({
  name: 'userNameFormat'
})
export class UserNameFormatPipe implements PipeTransform {
    constructor(private commonShare: CommonService) {}

  	transform(userId: string): string {
		try {
            let user = this.commonShare.userList.find(person => person.id == userId);

            console.log(user);
            
            if (user) {
                return user.name;
            }
		} catch (error) {
			return "?";
		}
  	}

}