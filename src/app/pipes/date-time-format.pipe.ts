import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateFormat'
})
export class DateFormatPipe implements PipeTransform {

  	transform(value: string): string {
		  console.log(value)
		try {
			let splited = value.split(" ");
			let splitDate = splited[0].split("-");
			let output = splitDate[2] + "." + splitDate[1] + "." + splitDate[0];
			return output;
		} catch (error) {
			return "?";
		}
  	}

}

@Pipe({
	name: 'timeFormat'
})
export class TimeFormatPipe implements PipeTransform {

	transform(value: string): string {
		try {
			let splited = value.split(" ");
			return splited[1];
		} catch (error) {
			return "?";
		}
	}

}