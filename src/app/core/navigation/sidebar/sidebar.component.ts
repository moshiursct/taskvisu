import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ProjectService } from 'src/app/services/http/project.service';
import { CommonService } from './../../../services/shared/common.service';

@Component({
	selector: 'app-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
	sidebar: Array<any>[];
	projectList: Array<any> = [];
	activeBar: number = 1;
	projectId: number;
	projectStatusId: number;
	projectStatus: string;
	route: string;
	userId: any;

	public parentList = {
		// teams: [],
		// meeting: [],
		// project: [],
		// regularMeeting: []
	};

	commonSidebar: Array<any> = [
		{
			"id": 1,
			"code": "open",
			"key": "offen",
			"startDate": '01.01.2019 00:00',
			'endDate': '10.10.2050 00:00',
			"name": "Open Task"
		},
		{
			"id": 2,
			"code": "overdue",
			"key": "offen",
			"startDate": '01.01.2019 00:00',
			'endDate': '10.10.2020 00:00',
			"name": "This Week & Overdue"
		},
		{
			"id": 3,
			"code": "next-week",
			"key": "offen",
			"startDate": '01.01.2019 00:00',
			'endDate': '07.10.2020 00:00',
			"name": "Next Week"
		},
		{
			"id": 4,
			"code": "all",
			"key": "all",
			"startDate": '01.01.2019 00:00',
			'endDate': '01.01.2050 00:00',
			"name": "All Task"
		},
		{
			"id": 5,
			"code": "closed",
			"key": "closed",
			"startDate": '01.01.2019 00:00',
			'endDate': '01.01.2020 00:00',
			"name": "All Closed Task"
		}
	];

	sprintOptions: Array<any> = [
		{
			"id": 1,
			"title": "Product Backlog",
			"code": "product_backlog",
			"key": "product_backlog",
			"startDate": '01.01.2019 00:00',
			'endDate': '10.10.2050 00:00',
			"name": "Open Task"
		},
		{
			"id": 2,
			"title": "Sprint Backlog",
			"code": "sprint_backlog",
			"key": "sprint_backlog",
			"startDate": '01.01.2019 00:00',
			'endDate': '10.10.2050 00:00',
			"name": "Open Task"
		},
		{
			"id": 3,
			"title": "Sprint",
			"code": "sprint",
			"key": "sprint",
			"startDate": '01.01.2019 00:00',
			'endDate': '10.10.2050 00:00',
			"name": "Open Task"
		}
	];

	constructor(
		public translate: TranslateService,
		public commonShare: CommonService,
		public projectService: ProjectService,
		private router: Router) { 
		this.userId = this.commonShare.currentUser.id;
	}

	ngOnInit(): void {
		this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				const page = this.router.url.replace('/', '');
				this.commonShare.taskof = this.findTaskOf(page);
				switch (page) {
					case 'my_task':
						break;

					case 'team_task':
						break;

					case 'assigned_task':
						break;

					case 'meeting':
						break;

					case 'project':
						this.handleProjectRoute();
						break;

					case 'regular_meeting':
						break;

					default:
						break;
				}
			}
		});
	}

	findTaskOf(page: string): number {
		const of = {
			my_task: 1,
			assigned_task: 1,
			team_task: 2,
			meeting: 3,
			project: 4,
			regular_meeting: 5
		};

		return of[page];
	}

	handleProjectRoute(): void {
		this.selectStatus(1, "offen");

		this.projectService.dataChanged.subscribe(data => {
			this.selectStatus(this.projectStatusId, this.projectStatus);
		});
	}

	filterTask(bar: any): void {
		this.activeBar = bar.id;
		this.commonShare.onSidebarCommonFilter(bar);
	}

	selectProject(id: any): void {
		this.projectId = id;
		this.getProjectTasksList(id);
	}

	selectStatus(id: number, status: string): void {
		this.projectStatusId = id;
		this.projectStatus = status;
		this.getProjectList(status);
	}

	getProjectList(status: string) {
		this.projectService.getList(this.userId, status).subscribe(projects => {
			this.projectList = projects;
		});
	}

	getProjectTasksList(id) {

	}

	openProjectDetails(data: any): void {
		this.projectService.onProjectClicked(data);
	}

}
