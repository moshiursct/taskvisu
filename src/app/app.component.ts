import { Component, Input, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { CommonHttpService } from './services/http/common.service';
import { CommonService } from './services/shared/common.service';
import { User } from './models/user.model';
import { Module } from './models/module.model';
import { TaskType } from './models/task-type.model';
import { Team } from './models/team.model';
import { Sprint } from './models/sprint.model';
import { TaskStatus } from './models/task-status.model';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

	constructor(
		private titleService: Title,
		private router: Router,
		private route: ActivatedRoute,
		private translate: TranslateService,
		private commonHttp: CommonHttpService,
		private commonShare: CommonService
	) {
		const currentLang = this.getCurrentLang();
		translate.setDefaultLang(currentLang);
		translate.use(currentLang);

		commonHttp.getPersonList().subscribe(persons => {
			commonShare.userList = persons.filter(person => person.userType != 'Guest');
		});

		this.getOptionsList();

		commonHttp.getTaskType().subscribe((taskTypes: Array<TaskType>) => {
			commonShare.taskTypeList = taskTypes.map(({ id, typeName, isDeletable, componentPermission }) => {
				let changedType = {
					id,
					typeName,
					isDeletable,
					permissions: {}
				};

				componentPermission.map(({ multiLangKey, name, model, isChecked, isMandatory }) => {
					changedType.permissions[model] = { name, multiLangKey, isChecked, isMandatory };
				});

				return changedType;
			});
		});

		commonHttp.getTeams().subscribe((teams: Array<Team>) => {
			commonShare.teamList = teams;
		});

		commonHttp.getSprints().subscribe((sprints: Array<Sprint>) => {
			commonShare.sprintList = sprints;
		});

		commonHttp.getStatusList().subscribe((taskStatusList: Array<TaskStatus>) => {
			commonShare.taskStatusList = taskStatusList
		});

		commonHttp.getUserTimerData().subscribe(data => {
			commonShare.taskTimeData = data;
		})
	}


	ngOnInit() {
		this.checkRouter();
	}

	checkRouter() {
		this.router.events.subscribe(event => {
			switch (true) {
				case event instanceof NavigationEnd:
					this.titleService.setTitle(this.route.firstChild.snapshot.data.title);
					this.commonShare.route = this.route.firstChild.snapshot.routeConfig.path;
					break;

				default:
					break;
			}
		})
	}

	getCurrentLang() {
		// const name = 'sct_language';
		// const nameEQ = name + "=";
		// const ca = document.cookie.split(';');
		// for (var i = 0; i < ca.length; i++) {
		//     var c = ca[i];
		//     while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		//     if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		// }
		return 'en';
	}

	getOptionsList() {
		this.commonHttp.getOptionsList().subscribe((data: any) => {
			data.map((item) => {
				if (item.type == "module") {
					let module: Module = new Module(item.id, item.name, item.type, item.createdAt, item.updatedAt);
					this.commonShare.moduleList.push(module);
				} else if (item.type == "client") {
					this.commonShare.clientList.push(item);
				} else {
					this.commonShare.categoryList.push(item);
				}
			})
		});
	}
}


