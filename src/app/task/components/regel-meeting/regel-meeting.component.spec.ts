import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegelMeetingComponent } from './regel-meeting.component';

describe('RegelMeetingComponent', () => {
  let component: RegelMeetingComponent;
  let fixture: ComponentFixture<RegelMeetingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegelMeetingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegelMeetingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
