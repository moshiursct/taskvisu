import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/shared/common.service';
import { ProjectService } from 'src/app/services/http/project.service';
@Component({
	selector: 'app-project',
	templateUrl: './project.component.html',
	styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
	public isWindowOpened: boolean = false;
	public isClosePressed: boolean = false;
	public isNew: boolean;
	public windowTop: number = 50;

	public currentProject: any = {};

	constructor(
		private commonService: CommonService,
		private projectService: ProjectService
	) { }

	ngOnInit(): void {
		this.projectService.projectClicked.subscribe(project => {
			this.isNew = false;
			this.isWindowOpened = true;
			this.currentProject = project;
		});
	}
	
	openWindow(): void {
		this.isNew = true;
		this.currentProject = undefined; 
		this.isWindowOpened = true;
	}
	
	closeWindow(): void {
		this.isClosePressed = true;
	}
}
