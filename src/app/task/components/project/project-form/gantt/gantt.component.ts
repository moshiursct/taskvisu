import { Component, Input } from '@angular/core';
import * as moment from 'moment';
import { Project } from 'src/app/models/project.model';
import { GanttService } from 'src/app/services/http/gantt.service';
import { CommonService } from 'src/app/services/shared/common.service';

@Component({
	selector: 'app-gantt',
	templateUrl: './gantt.component.html',
	styleUrls: ['./gantt.component.css']
})
export class GanttComponent {
	@Input() isNewGantt: boolean;
	@Input() currentProject: Project;

	public isClosePressed: boolean = false;
	public isWindowOpened: boolean = false;
	public windowTop: number = 50;
	public windowLeft: number = 200;

	private lang = "default";
	private midDate = new Date();
	private startMonthDiff = 2;
	private endMonthDiff = 2;

	public monthsList = [];
	public weeksList = [];

	private localeOptions = {
		month: 'long',
		year: 'numeric'
	}

	public initialDayDiff = 0;

	public ganttList = [
		// {
		// 	id: 1,
		// 	phase: "Test",
		// 	task: "Task 1",
		// 	responsiblePerson: "Tanbir, Mahin",
		// 	responsibleNr: "101006",
		// 	personNr: "101006",
		// 	status: "Done",
		// 	progress: 100,
		// 	details: "<b>Task Details 1</b>",
		// 	startDate: "2020-10-04",
		// 	endDate: "2020-10-07"
		// },
		// {
		// 	id: 2,
		// 	phase: "Test",
		// 	task: "Task 2",
		// 	responsiblePerson: "MD. Ashraful Alam",
		// 	responsibleNr: "101004",
		// 	personNr: "101004",
		// 	status: "In Progress",
		// 	progress: 50,
		// 	details: "<b>Task Details 2</b>",
		// 	startDate: "2020-11-23",
		// 	endDate: "2020-12-10"
		// },
		// {
		// 	id: 3,
		// 	phase: "Check",
		// 	task: "Fix Bugs",
		// 	responsiblePerson: "Tanbir, Mahin",
		// 	responsibleNr: "101006",
		// 	personNr: "101006",
		// 	status: "In Progress",
		// 	progress: 25,
		// 	details: "<b>Task Details 3</b>",
		// 	startDate: "2020-10-22",
		// 	endDate: "2020-10-27"
		// }
	];

	public phaseList = [];
	public phaseData = [];

	public currentTaskID: number;
	public currentGantt = {};

	public availableDates = {
		start: [],
		end: []
	};

	constructor(public ganttService: GanttService, public commonShare: CommonService) { }

	ngOnInit(): void {
		this.getGanttList();
	}

	initGantt() {
		this.monthsList = [];
		this.weeksList = [];

		for (let i = -this.startMonthDiff; i <= this.endMonthDiff; i++) {
			const month = moment(this.midDate).add(i, "months").endOf("month").toDate().toLocaleString(this.lang, this.localeOptions);

			this.monthsList.push(month);
		}

		let firstDate = new Date(this.monthsList[0]);
		let initialDay = moment(firstDate).startOf('week').toString();

		this.initialDayDiff = moment(firstDate).diff(initialDay, 'days');

		const lastDate = new Date(this.monthsList[this.monthsList.length - 1]);
		const from = new Date(firstDate.getFullYear(), firstDate.getMonth(), 1);
		const to = new Date(lastDate.getFullYear(), lastDate.getMonth() + 1, 0);

		let [start, end, startYear, endYear] = this.getWeekBetweenDates(from, to);

		let isWeek = true;

		while (isWeek) {
			this.weeksList.push(parseInt(start));

			if (start == end && startYear == endYear) {
				isWeek = false;
			}

			if (this.weeksInYear(startYear) == 52 && start == 52) {
				start = 0;
				startYear++;

				if (startYear > endYear) {
					startYear--;
				}
			} else if (this.weeksInYear(startYear) == 53 && start == 53) {
				start = 0;
				startYear++;

				if (startYear > endYear) {
					startYear--;
				}
			}

			start++;
		}
	}

	getGanttList() {
		let userId = this.commonShare.currentUser.id;
		let projectId = this.currentProject?.id;

		if (projectId) {
			this.ganttService.getList(projectId, userId).subscribe(gantts => {
				this.ganttList = gantts;

				this.availableDates = {
					start: [],
					end: []
				};

				this.ganttList.map(item => {
					this.availableDates.start.push(item.startDate);
					this.availableDates.end.push(item.endDate);
				});

				let startMoments = this.availableDates.start.map(d => moment(d));
				let minDate = moment.min(startMoments);

				let endMoments = this.availableDates.end.map(d => moment(d));
				let maxDate = moment.max(endMoments);

				let dayDiff = moment(maxDate).diff(minDate, 'days') / 2;
				let finalMidDate = moment(minDate).add(dayDiff, "days").toDate();
				let monthDiff = moment(finalMidDate).diff(minDate, 'months');

				this.startMonthDiff = monthDiff + 2;
				this.endMonthDiff = monthDiff + 2;
				this.midDate = finalMidDate;

				this.initGantt();
				this.reMapGantt();
			});
		} else {
			this.initGantt();
			this.reMapGantt();
		}
	}

	reMapGantt() {
		this.phaseList = [];
		this.phaseData = [];

		this.ganttList.map(item => {
			if (this.phaseList.includes(item.phase)) {
				const index = this.phaseData.findIndex(p => p.name == item.phase);
				this.phaseData[index].tasks.push(item);
			} else {
				this.phaseList.push(item.phase);
				this.phaseData.push({
					name: item.phase,
					tasks: [item]
				});
			}
		});

		this.ganttService.phaseList = [];

		for (let i = 0; i < this.phaseList.length; i++) {
			this.ganttService.phaseList.push({ name: this.phaseList[i] });
		}

		setTimeout(() => {
			document.querySelectorAll(".gantt-progress-wrapper").forEach((div: HTMLElement) => {
				this.dragElement(div);
				this.resizeLeftHandle(div);
				this.resizeRightHandle(div);
			});

			this.ganttList.map(item => {
				let totalLeftWidth = 0;
				let totalWidth = 0;

				let initDayDiff = moment(item.startDate).diff(moment(this.monthsList[0]), 'days');
				let startEndDayDiff = moment(item.endDate).diff(moment(item.startDate), 'days');

				totalLeftWidth = (initDayDiff / 7) * 57.5;
				totalWidth = (startEndDayDiff / 7) * 57.5;

				let gantt_progress: HTMLElement = document.querySelector(".gantt_" + item.id);
				gantt_progress.style.left = totalLeftWidth + "px";
				gantt_progress.style.width = totalWidth + "px";
			});
		})
	}

	selectTask(task: any) {
		this.currentTaskID = task.id;
	}

	taskDblClick(task: any) {
		this.currentGantt = { ...task };
		this.ganttService.currentPhase = task.phase;
		this.isNewGantt = false;
		this.isWindowOpened = true;
	}

	weeksInYear(year: number) {
		return Math.max(
			moment(new Date(year, 11, 31)).isoWeek(),
			moment(new Date(year, 11, 31 - 7)).isoWeek()
		);
	}

	ngAfterViewInit(): void {
		this.ganttService.dataChanged.subscribe((data: any) => {
			data.startDate = moment(data.startDate).format("YYYY-MM-DD");
			data.endDate = moment(data.endDate).format("YYYY-MM-DD");
			
			console.log(data);
			
			const getIndex = this.ganttList.findIndex(item => item.id == data.id);

			if (getIndex > -1) {
				this.ganttList[getIndex] = { ...data };
			} else {
				this.ganttList.push(data);
			}

			console.log(this.ganttList);

			this.reMapGantt();
		});
	}

	getMonthStyle(month: string): any {
		return {
			'min-width': (moment(month).daysInMonth() / 7) * 57.5 + "px",
			'width': (moment(month).daysInMonth() / 7) * 57.5 + "px"
		}
	}

	getWeekStyle(weekIndex: number): any {
		const firstWeekStyle = this.initialDayDiff != 0 ? 57.5 / 7 * (7 - this.initialDayDiff) : 0;

		return {
			'min-width': weekIndex == 0 ? firstWeekStyle + 'px' : '57.5px',
			'width': weekIndex == 0 ? firstWeekStyle + 'px' : 100 / this.weeksList.length + '%'
		}
	}

	resizeLeftHandle(element) {
		const that = this;

		var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;

		element.children[1].onmousedown = dragMouseDown;

		function dragMouseDown(e) {
			e = e || window.event;
			e.preventDefault();

			pos3 = e.clientX;
			pos4 = e.clientY;

			document.onmouseup = closeDragElement;
			document.onmousemove = elementDrag;
		}

		function elementDrag(e) {
			e = e || window.event;
			e.preventDefault();

			pos1 = pos3 - e.clientX;
			pos2 = pos4 - e.clientY;
			pos3 = e.clientX;
			pos4 = e.clientY;

			if (element.getBoundingClientRect().width + pos1 >= 6) {
				element.style.left = (element.offsetLeft - pos1) + "px";
				element.style.width = (element.getBoundingClientRect().width + pos1) + "px";
			}

			if (element.offsetLeft < 6) {
				element.style.left = (element.offsetLeft + 250) + "px";
				that.startMonthDiff++;
				that.initGantt();
			}
		}

		function closeDragElement() {
			document.onmouseup = null;
			document.onmousemove = null;

			that.onBarChanged(element);
		}
	}

	resizeRightHandle(element) {
		const that = this;

		var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;

		element.children[2].onmousedown = dragMouseDown;

		function dragMouseDown(e) {
			e = e || window.event;
			e.preventDefault();

			pos3 = e.clientX;
			pos4 = e.clientY;

			document.onmouseup = closeDragElement;
			document.onmousemove = elementDrag;
		}

		function elementDrag(e) {
			e = e || window.event;
			e.preventDefault();

			pos1 = pos3 - e.clientX;
			pos2 = pos4 - e.clientY;
			pos3 = e.clientX;
			pos4 = e.clientY;

			if (element.getBoundingClientRect().width - pos1 >= 6) {
				element.style.width = (element.getBoundingClientRect().width - pos1) + "px";
			}

			let gantt_week_data_container = document.querySelector(".gantt-week-data-container");
			let gantt_calendar = document.querySelector(".gantt-calendar");

			let innerDivWidth = element.getBoundingClientRect().right - gantt_week_data_container.getBoundingClientRect().right;

			if (innerDivWidth > -10) {
				that.endMonthDiff++;
				that.initGantt();

				setTimeout(() => {
					gantt_calendar.scrollLeft = gantt_week_data_container.scrollWidth;
				});
			}
		}

		function closeDragElement() {
			document.onmouseup = null;
			document.onmousemove = null;

			that.onBarChanged(element);
		}
	}

	onBarChanged(element: HTMLElement): void {
		const width = element.getBoundingClientRect().width;
		const left = element.offsetLeft;

		let monthFirstDate = new Date(this.monthsList[0]);
		let startDuration = left / 8.2;
		let currentStartDate = moment(monthFirstDate).add(startDuration, "days").toDate();

		let endDuration = width / 8.2;
		let currentEndDate = moment(currentStartDate).add(endDuration, "days").toDate();

		this.ganttList.map(item => {
			if (item.id == parseInt(element.dataset.rowId)) {
				item.startDate = moment(currentStartDate).format("YYYY-MM-DD");
				item.endDate = moment(currentEndDate).format("YYYY-MM-DD");
			}
		});
	}

	dragElement(element) {
		const that = this;

		var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;

		if (element.firstChild) {
			element.firstChild.onmousedown = dragMouseDown;
		} else {
			element.onmousedown = dragMouseDown;
		}

		function dragMouseDown(e) {
			e = e || window.event;
			e.preventDefault();
			e.stopPropagation();

			pos3 = e.clientX;
			pos4 = e.clientY;

			document.onmouseup = closeDragElement;
			document.onmousemove = elementDrag;
		}

		function elementDrag(e) {
			e = e || window.event;
			e.preventDefault();

			pos1 = pos3 - e.clientX;
			pos2 = pos4 - e.clientY;
			pos3 = e.clientX;
			pos4 = e.clientY;

			element.style.left = (element.offsetLeft - pos1) + "px";

			if (element.offsetLeft < 6) {
				element.style.left = (element.offsetLeft + 250) + "px";
				that.startMonthDiff++;
				that.initGantt();
			}

			let gantt_week_data_container = document.querySelector(".gantt-week-data-container");
			let gantt_calendar = document.querySelector(".gantt-calendar");

			let innerDivWidth = element.getBoundingClientRect().right - gantt_week_data_container.getBoundingClientRect().right;

			if (innerDivWidth > -10) {
				that.endMonthDiff++;
				that.initGantt();

				setTimeout(() => {
					gantt_calendar.scrollLeft = gantt_week_data_container.scrollWidth;
				});
			}
		}

		function closeDragElement() {
			document.onmouseup = null;
			document.onmousemove = null;

			that.onBarChanged(element);
		}
	}

	private getWeekBetweenDates(from, to): any {
		return [
			moment(from).format('WW'),
			moment(to).format('WW'),
			moment(from).format('Y'),
			moment(to).format('Y')
		];
	}

	openWindow(): void {
		this.currentGantt = {};
		this.isNewGantt = true;
		this.isWindowOpened = true;
	}

	closeWindow() {
		this.isClosePressed = true;
	}
}

