import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UploadComponent } from '@progress/kendo-angular-upload';
import { objectEquals } from 'src/app/helpers/helper';
import { Project } from 'src/app/models/project.model';
import { CommonHttpService } from 'src/app/services/http/common.service';
import { GanttService } from 'src/app/services/http/gantt.service';
import { CommonService } from 'src/app/services/shared/common.service';
import { NotificationService } from 'src/app/services/shared/notification.service';

@Component({
	selector: 'app-gantt-form',
	templateUrl: './gantt-form.component.html',
	styleUrls: ['./gantt-form.component.css']
})
export class GanttFormComponent implements OnInit {
	@Input() isNewGantt: boolean;
	@Input() isClosePressed: boolean;
	@Input() currentProject: Project;
	@Input() currentGantt: any;

	@Output() isGanttWindowOpened: EventEmitter<any> = new EventEmitter<any>();

	public ganttForm: FormGroup;
	public isWarningModalOpen: boolean = false;
	public fileCount: number = 0;
	public fileUploader: UploadComponent;

	public deletedFiles: Array<any> = [];

	public ganttData = {};
	public ganttDataCopy = {};

	constructor(
		private notificationService: NotificationService,
		private commonHttpService: CommonHttpService,
		private commonShare: CommonService,
		private ganttService: GanttService,
		private changeDetectorRef: ChangeDetectorRef
	) {
		this.saveChangedData = this.saveChangedData.bind(this);
		this.cancelChangedWarning = this.cancelChangedWarning.bind(this);
	}

	ngOnInit(): void {
		this.ganttForm = new FormGroup({});
	}

	ngOnChanges(changes: SimpleChanges) {
		setTimeout(() => {
			if (changes.isClosePressed.currentValue) {
				this.closeWindow();
			}
		});
	}

	ngAfterViewInit(): void {
		if (!this.isNewGantt) {
			this.ganttForm.patchValue({ 
				phase: this.currentGantt.phase,
				task: this.currentGantt.task,
				startDate: new Date(this.currentGantt.startDate),
				endDate: new Date(this.currentGantt.endDate),
				details: this.currentGantt.details,
				personNr: this.currentGantt.personNr,
				responsibleNr: this.currentGantt.responsibleNr,
				progress: this.currentGantt.progress
			});
		} else {
			this.ganttForm.patchValue({
				personNr: this.commonShare.currentUser.id.toString()
			});
		}

		this.changeDetectorRef.detectChanges();
		this.ganttData = { ...this.ganttForm.value };
	}

	closeWindow() {
		if (objectEquals(this.ganttData, this.ganttForm.value)) {
			this.isGanttWindowOpened.emit(false);
		} else {
			this.isWarningModalOpen = true;
		}
	}

	saveChangedData(): void {
		this.submit({});
		this.isWarningModalOpen = false;
	}

	cancelChangedWarning(): void {
		this.isWarningModalOpen = false;
		this.isGanttWindowOpened.emit(false);

		// this.ganttForm.patchValue({ ...this.ganttData });
	}

	uploadCompleteEvent(data: any): void {
		this.prepare(data);

		console.log(data);
	}

	fileDeleteEvent(data: any): void {
		this.deletedFiles.push(data);
	}

	prepare(attachments: Array<any>) {
		const payload = {
			...this.ganttForm.getRawValue(),
			attachments,
			id: this.isNewGantt ? 0 : this.currentGantt.id,
			currentUser: this.commonShare.currentUser.id
		};

		if (this.deletedFiles.length > 0) {
			this.commonHttpService.deleteAttachments(this.deletedFiles).subscribe(response => {
				if (response) {
					this.save(payload);
				} else {
					this.notificationService.showErrorMsg("Error occured deleting the attachment(s)!");
				}
			});
		} else {
			this.save(payload);
		}
	}

	save(payload: any): void {
		const projectId = !this.currentProject ? 0 : this.currentProject.id;

		this.ganttService.save(payload, projectId).subscribe(response => {
			if (response) {
				payload.id = response;
				payload.phase = this.isNewGantt ? payload.phase : payload.setPhaseNr;
				
				this.notificationService.showSuccessMsg("Gantt saved successfully.");
				this.ganttService.onDataChanged(payload);
				this.isGanttWindowOpened.emit(false);
			} else {
				this.notificationService.showErrorMsg("Failed to save Gantt Data.");
			}
		});
	}

	submit(e: any): void {
		// console.log(this.currentProject);

		// return;

		if (!this.ganttForm.value.phase) {
			this.notificationService.showErrorMsg("Phase can't be empty!");
			return;
		}

		if (!this.ganttForm.value.task) {
			this.notificationService.showErrorMsg("Task name can't be empty!");
			return;
		}

		if (!this.ganttForm.value.responsibleNr) {
			this.notificationService.showErrorMsg("Please choose a responsible person!");
			return;
		}

		if (this.fileCount) {
			this.fileUploader.uploadFiles();
		} else {
			this.prepare([]);
		}
	}

}
