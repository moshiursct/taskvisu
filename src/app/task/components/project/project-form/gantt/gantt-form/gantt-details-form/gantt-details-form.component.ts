import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DropDownFilterSettings } from '@progress/kendo-angular-dropdowns';
import { GanttService } from 'src/app/services/http/gantt.service';
import { CommonService } from 'src/app/services/shared/common.service';

@Component({
	selector: 'app-gantt-details-form',
	templateUrl: './gantt-details-form.component.html',
	styleUrls: ['./gantt-details-form.component.css']
})
export class GanttDetailsFormComponent implements OnInit {
	@Input() isNewGantt: boolean;
	@Input() ganttForm: FormGroup;

	public phaseDisabled: boolean = false;
	public setPhaseDisabled: boolean = false;

	public filterSettings: DropDownFilterSettings = {
		caseSensitive: false,
		operator: 'contains'
	};

	public userList: Array<any> = [];
	public phaseList: Array<any> = [];
	public orderList: Array<any> = [];
	public datePlaceHolder: Object = { 'year': '', 'month': '', 'day': '' };

	public currentPhase: string = "";

	constructor(private commonShare: CommonService, public ganttService: GanttService) {
		this.setDatePlaceHolder();
	}

	ngOnInit(): void {
		this.userList = [...this.commonShare.userList];
		this.orderList = [...this.commonShare.userList];

		this.ganttForm.addControl("phase", new FormControl(""));
		this.ganttForm.addControl("task", new FormControl(""));
		this.ganttForm.addControl("details", new FormControl(""));
		this.ganttForm.addControl("startDate", new FormControl(new Date()));
		this.ganttForm.addControl("endDate", new FormControl(new Date()));
		this.ganttForm.addControl("personNr", new FormControl({ value: 0, disabled: true }));
		this.ganttForm.addControl("setPhaseNr", new FormControl(""));
		this.ganttForm.addControl("setOrderNr", new FormControl(0));
		this.ganttForm.addControl("responsibleNr", new FormControl(0));
		this.ganttForm.addControl("progress", new FormControl(0));
	}

	ngAfterViewInit(): void {
		this.phaseList = [...this.ganttService.phaseList];

		if (this.isNewGantt) {
			this.setPhaseDisabled = true;
			this.phaseDisabled = false;
		} else {
			this.setPhaseDisabled = false;
			this.phaseDisabled = true;

			this.currentPhase = this.ganttService.currentPhase;

			console.log(this.currentPhase);

			this.ganttForm.patchValue({
				setPhaseNr: this.currentPhase
			})
		}
	}

	setDatePlaceHolder(): void {
		const today = new Date();
		const year: number = today.getFullYear();
		let month: any = (today.getMonth() + 1);
		let day: any = today.getDate();
		month = month < 10 ? `0${month}` : month;
		day = day < 10 ? `0${day}` : day;
		this.datePlaceHolder = { year, month, day };
	}
}
