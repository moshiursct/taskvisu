import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UploadComponent } from '@progress/kendo-angular-upload';
import { ProjectService } from 'src/app/services/http/project.service';
import { CommonHttpService } from 'src/app/services/http/common.service';
import { NotificationService } from 'src/app/services/shared/notification.service';
import { CommonService } from 'src/app/services/shared/common.service';
import { objectEquals } from '../../../../helpers/helper';
import { Project } from '../../../../models/project.model';

@Component({
	selector: 'app-project-form',
	templateUrl: './project-form.component.html',
	styleUrls: ['./project-form.component.css']
})
export class ProjectFormComponent implements OnInit {
	@Input() isNew: boolean;
	@Input() isClosePressed: boolean;
	@Input() currentProject: Project;

	@Output() isProjectWindowOpened: EventEmitter<any> = new EventEmitter<any>();

	public projectForm: FormGroup;
	public isWarningModalOpen: boolean = false;
	public fileCount: number = 0;
	public fileUploader: UploadComponent;

	public deletedFiles: Array<any> = [];

	public projectData = {};
	public projectDataCopy = {};

	constructor(
		private notificationService: NotificationService,
		private commonHttpService: CommonHttpService,
		private commonShare: CommonService,
		private projectService: ProjectService,
		private changeDetectorRef: ChangeDetectorRef
	) {
		this.saveChangedData = this.saveChangedData.bind(this);
		this.cancelChangedWarning = this.cancelChangedWarning.bind(this);
	}

	ngOnInit(): void {
		this.projectForm = new FormGroup({});
	}

	ngOnChanges(changes: SimpleChanges) {
		setTimeout(() => {
			if (changes.isClosePressed.currentValue) {
				this.closeWindow();
			}
		});
	}

	ngAfterViewInit(): void {
		if (!this.currentProject) {
			this.projectForm.patchValue({
				name: "",
				progress: 0,
				personNr: this.commonShare.currentUser.id.toString(),
				partsNr: [],
				participantsNr: [],
				date: new Date(),
				details: "",
				attachments: [],
				isArchived: false
			});
		} else {
			const { name, participantsNr, personNr, partsNr, progress, details, date, attachments, isArchived } = this.currentProject;

			this.projectForm.patchValue({
				name,
				progress,
				personNr: personNr.toString(),
				partsNr: partsNr.split(","),
				participantsNr: participantsNr.split(","),
				date: new Date(date),
				details: unescape(details),
				attachments,
				isArchived: isArchived ? true : false
			});
		}

		this.changeDetectorRef.detectChanges();
		this.projectData = { ...this.projectForm.value };
	}

	closeWindow() {
		if (objectEquals(this.projectData, this.projectForm.value)) {
			this.isProjectWindowOpened.emit(false);
		} else {
			this.isWarningModalOpen = true;
		}
	}

	saveChangedData(): void {
		this.submit({});
		this.isWarningModalOpen = false;
	}

	cancelChangedWarning(): void {
		this.isWarningModalOpen = false;
		this.isProjectWindowOpened.emit(false);

		// this.projectForm.patchValue({ ...this.projectData });
	}

	uploadCompleteEvent(data: any): void {
		this.prepare(data);

		console.log(data);
	}

	fileDeleteEvent(data: any): void {
		this.deletedFiles.push(data);
	}

	prepare(attachments: Array<any>) {
		const payload = {
			...this.projectForm.value,
			attachments,
			id: this.isNew ? 0 : this.currentProject.id,
			currentUser: this.commonShare.currentUser.id
		};

		if (this.deletedFiles.length > 0) {
			this.commonHttpService.deleteAttachments(this.deletedFiles).subscribe(response => {
				if (response) {
					this.save(payload);
				} else {
					this.notificationService.showErrorMsg("Error occured deleting the attachment(s)!");
				}
			});
		} else {
			this.save(payload);
		}
	}

	save(payload: any): void {
		this.projectService.save(payload).subscribe(response => {
			if (response) {
				this.notificationService.showSuccessMsg("Project saved successfully.");
				this.projectService.onDataChanged(1);
				this.isProjectWindowOpened.emit(false);
			} else {
				this.notificationService.showErrorMsg("Failed to save Project Data.");
			}
		});
	}

	submit(e: any): void {
		if (!this.projectForm.value.name) {
			this.notificationService.showErrorMsg("Project Name can't be empty!");
			return;
		}

		if (this.fileCount) {
			this.fileUploader.uploadFiles();
		} else {
			this.prepare([]);
		}
	}
}
