import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DropDownFilterSettings } from '@progress/kendo-angular-dropdowns';
import { CommonService } from 'src/app/services/shared/common.service';

@Component({
	selector: 'app-project-details-form',
	templateUrl: './project-details-form.component.html',
	styleUrls: ['./project-details-form.component.css']
})
export class ProjectDetailsFormComponent implements OnInit {
	@Input() new: boolean;
	@Input() projectForm: FormGroup;

	public filterSettings: DropDownFilterSettings = {
		caseSensitive: false,
		operator: 'contains'
	};

	public userList: Array<any> = [];
	public partsList: Array<any> = [];
	public datePlaceHolder: Object = { 'year': '', 'month': '', 'day': '' };

	constructor(private commonShare: CommonService) {
		this.setDatePlaceHolder();
	}

	ngOnInit(): void {
		this.userList = [...this.commonShare.userList];
		this.partsList = [];

		this.projectForm.addControl("name", new FormControl(""));
		this.projectForm.addControl("details", new FormControl(""));
		this.projectForm.addControl("date", new FormControl(new Date()));
		this.projectForm.addControl("personNr", new FormControl(0));
		this.projectForm.addControl("participantsNr", new FormControl([]));
		this.projectForm.addControl("partsNr", new FormControl([]));
		this.projectForm.addControl("progress", new FormControl(0));
	}

	ngAfterViewInit(): void {

	}

	setDatePlaceHolder(): void {
		const today = new Date();
		const year: number = today.getFullYear();
		let month: any = (today.getMonth() + 1);
		let day: any = today.getDate();
		month = month < 10 ? `0${month}` : month;
		day = day < 10 ? `0${day}` : day;
		this.datePlaceHolder = { year, month, day };
	}
}
