import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { CellClickEvent, DataStateChangeEvent, GridDataResult, RowClassArgs, SelectableSettings } from '@progress/kendo-angular-grid';
import { DevStatus } from "../../../../models/dev-status.model";
import { CommonService } from "../../../../services/shared/common.service";
import { DevStatusService } from "../../../../services/http/dev-status.service";
import { NotificationService } from '../../../../services/shared/notification.service';
import { process, State } from '@progress/kendo-data-query';

@Component({
	selector: 'app-dev-status',
	templateUrl: './dev-status.component.html',
	styleUrls: ['./dev-status.component.css']
})
export class DevStatusComponent implements OnInit {
	@Input() isNewTask: boolean = false;
	@Output() allDevStatus = new EventEmitter<any>();
	@ViewChild('devStatusGrid') private devStatusGrid;
	gridData: GridDataResult;

	public devStatusList: DevStatus[] = [];
	selectedRequirement: DevStatus;
	removingRequirement: number;
	// Dev status dialogs variable
	public windowOpened = false;
	public windowTop: number;
	public windowLeft: number;

	// Warning modal open variable
	warningOpened: boolean = false;
	currentTask: number;
	overallProgress: any = 0;

	// State For Filter
	public state: State = {
        skip: 0,
        take: 5,

        // Initial filter descriptor
        filter: {
          logic: 'and',
          filters: [{ field: 'requirement', operator: 'contains', value: '' }]
        }
    };

	constructor(
		private commonService: CommonService,
		private devStatusService: DevStatusService,
		private notificationService: NotificationService
	) { }

	ngOnInit(): void {
		if (this.isNewTask) {
			this.windowTop = 0;
			this.windowLeft = 20;

			this.devStatusList = [];
			this.allDevStatus.emit([]);
		} else {
			this.windowTop = 0;
			this.windowLeft = -500;
			this.currentTask = this.commonService.currentTask.id;
			if (this.currentTask) {
				this.getDevStatusList(this.currentTask);
			}
		}
	}

	getDevStatusList(currentTask) : void {
		this.devStatusService.getDevStatusList(currentTask).subscribe((data) => {
			this.devStatusList = [...data.records];
			console.log(this.state);
			
			this.gridData = process(this.devStatusList, this.state);
			this.calculateOverallProgress();
			// console.log(this.devStatusGridView);

		})
	}

	openRequirementAddWindow(event): void {
		this.selectedRequirement = undefined;
		this.windowOpened = true;
	}

	public close() : void {
		this.windowOpened = false;
	}

	saveDevStatus(event): void {
		if (this.validateRequirement(event)) {
			if (this.isNewTask) {
				// this.devStatusGrid.addRow(event);
				event.taskId = null;
				this.devStatusList.push(event);
				this.gridData = process(this.devStatusList, this.state);
				this.allDevStatus.emit(this.devStatusList);
				// this.devStatusGridView = process(this.devStatusList, this.state);
				this.notificationService.showSuccessMsg("Dev Status saved Successfully!!!");
				// console.log(this.devStatusGridView);
				this.close();
			} else {
				this.devStatusService.saveDevStatusData(event).subscribe(response => {
					if (response == 1) {
						if (event.id) {
							this.notificationService.showSuccessMsg("Dev Status Updated Successfully!!!");
						} else {
							this.notificationService.showSuccessMsg("Dev Status saved Successfully!!!");
						}
						this.getDevStatusList(this.currentTask);
						this.close();
					}
				})
			}
		} else {
			return;
		}
	}

	validateRequirement(requirement: DevStatus): boolean {
		let errorList: string[] = [];
		if (!requirement.requirement || requirement.requirement == "") {
			errorList.push("Requirement Required!!");
		}
		if (requirement.details == "") {
			errorList.push("User Story Required!!");
		}

		if (!requirement.feedbackBy.length) {
			errorList.push("Feedback person required!!");
		}

		if (!requirement.implementedIn.length) {
			errorList.push("At least One Implemented Server required!!");
		}

		if (errorList.length > 0) {
			for (let i = 0; i < errorList.length; i++) {
				this.notificationService.showErrorMsg(errorList[i]);
			}
			return false;
		}
		return true;
	}

	openRequirementEditWindow(requirement: DevStatus): void {
		this.selectedRequirement = requirement;
		this.windowOpened = true;
	}

	openRequirementDeleteWarning(requirementId: number): void {
		this.warningOpened = true;
		this.removingRequirement = requirementId;
	}

	devStatusStateChange(state: DataStateChangeEvent): void {
		this.state = state;
		console.log(this.state);
		
		this.gridData = process(this.devStatusList, this.state);
	}

	confirmDeleteRequirement = (): void => {
		this.devStatusService.deleteDevStatusData(this.removingRequirement)
			.subscribe(response => {
				if (response == 1) {
					this.warningOpened = false;
					this.getDevStatusList(this.currentTask);
					this.notificationService.showSuccessMsg('Dev Status Deleted Successfull!');
				} else {
					this.notificationService.showErrorMsg('Dev Status delete Failed!!!')
				}
			});
	}

	cancelDeleteRequirement = (): void => {
		this.warningOpened = false;
		this.removingRequirement = undefined;
	}

	calculateOverallProgress(): void {
		let requirementNo = this.devStatusList.length;
		let perRequirementprogress = 100 / requirementNo;
		let overallProgress: any = 0;
		const perProgress = 100 / 4;
		this.devStatusList.map(item => {
			let implementationProgress = 0;
			if (item.implementationProgress != null && item.implementationProgress) {
				const totalServer = item.implementedIn.length;
				for (const [key, value] of Object.entries(item.implementationProgress)) {
					if (value) {
						implementationProgress++;
					}
				}
				implementationProgress = (implementationProgress / totalServer) * 100;
			}
			let progress = 0;
			progress += (perProgress * item.designProgress) / 100;
			progress += (perProgress * item.developmentProgress) / 100;
			progress += item.initialTestProgress == 1 ? perProgress : 0;
			progress += item.feedbackProgress == 1 ? perProgress : 0;
			progress += (12.5 * item.releaseProgress) / 100;
			progress += (12.5 * implementationProgress) / 100;
			overallProgress += (perRequirementprogress * progress) / 100;
		})
		this.overallProgress = Math.ceil(overallProgress);
	}

	public rowCallback(context: RowClassArgs) {
		const isEven = context.index % 2 == 0;
		return {
			even: isEven,
			odd: !isEven
		};
	}

	public selectableSettings: SelectableSettings = {
		mode: 'single',
		drag: false
	};

	public onRowSelect(event: CellClickEvent) {
		// this.commonShare.onTaskRowClick(event);
	}

}
