import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevStatusFormComponent } from './dev-status-form.component';

describe('DevStatusFormComponent', () => {
  let component: DevStatusFormComponent;
  let fixture: ComponentFixture<DevStatusFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevStatusFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevStatusFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
