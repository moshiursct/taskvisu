import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RemoveTagEvent } from '@progress/kendo-angular-dropdowns/dist/es2015/common/remove-tag-event';
import { DevStatus } from '../../../../../models/dev-status.model';
import { DevStatusService } from 'src/app/services/http/dev-status.service';
import { NotificationService } from 'src/app/services/shared/notification.service';

import { CommonService } from "../../../../../services/shared/common.service";

@Component({
	selector: 'app-dev-status-form',
	templateUrl: './dev-status-form.component.html',
	styleUrls: ['./dev-status-form.component.css']
})
export class DevStatusFormComponent implements OnInit {

	constructor(
		private commonShare: CommonService,
		private notificationService: NotificationService,
		private devStatusService: DevStatusService
	) { }

	@Output() saveDevStatusEvent = new EventEmitter<DevStatus>();
	@Input() selectedDevStatus: DevStatus;


	public userList: {}[];
	public clientList: {}[];
	public oldData: [] = [];

	public numbers = [0, 25, 50, 75, 100];

	// Use a callback function to capture the 'this' execution context of the class.
	public title = value => {
		return this.numbers[value];
	};

	public devStatus: DevStatus = {
		id: null,
		taskId: this.commonShare.currentTask.id ? this.commonShare.currentTask.id : null,
		requirement: '',
		details: '',
		designedBy: [],
		designProgress: 0,
		developedBy: [],
		developmentProgress: 0,
		initialTestBy: [],
		initialTestProgress: 0,
		feedbackBy: [],
		feedbackProgress: 0,
		releasedBy: null,
		releaseProgress: 0,
		understanding: 0,
		implementedIn: [],
		implementationProgress: {}
	}

	ngOnInit(): void {
		this.userList = this.commonShare.userList;
		this.clientList = this.commonShare.clientList;
		if (this.selectedDevStatus) {
			this.devStatus = this.selectedDevStatus;
		}
		console.log(this.devStatus);
		
	}

	submitDevStatusData(event) {
		this.saveDevStatusEvent.emit(this.devStatus);
	}

	implementChange(event: []) {
		if (!event.length) {
			this.devStatus.implementationProgress = {};
		} else {
			let impProgressKeys = Object.keys(this.devStatus.implementationProgress);
			let clientName = event.map(item => this.getClientName(item));
	
			for(let i = 0; i < impProgressKeys.length; i++) {
			  	if (!clientName.includes(impProgressKeys[i])) {
					delete this.devStatus.implementationProgress[impProgressKeys[i]];
			  	}
			}
		}
	}

	removeTag(e: RemoveTagEvent) {
		if (e?.dataItem?.name) {
			delete this.devStatus.implementationProgress[e.dataItem.name];
		}
	}

	getClientName(id: number) {
		return this.clientList.find((item: { id: number, name: string }) => item.id == id)['name'];
	}



}
