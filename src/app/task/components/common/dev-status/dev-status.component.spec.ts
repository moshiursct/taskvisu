import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevStatusComponent } from './dev-status.component';

describe('DevStatusComponent', () => {
  let component: DevStatusComponent;
  let fixture: ComponentFixture<DevStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
