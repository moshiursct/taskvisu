import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { State, SortDescriptor } from '@progress/kendo-data-query'
import { GridDataResult, DataStateChangeEvent, CellClickEvent, SelectableSettings, RowClassArgs } from '@progress/kendo-angular-grid';
import { TaskService } from "../../../../services/http/task.service";
import { CommonService } from './../../../../services/shared/common.service';
@Component({
  selector: 'app-task-grid',
  templateUrl: './task-grid.component.html',
  styleUrls: ['./task-grid.component.css']
})
export class TaskGridComponent implements OnInit, AfterViewInit {
    @ViewChild('taskGrid') private taskGrid: ElementRef;
  public pageSizes: Array<any> = [20, 50, 100, 200];
  public view:Observable<GridDataResult>;
  public state: State = {
    skip: 0,
    take: 50,
  }

    public multiple = false;
    public allowUnsort = true;
    public defaultSelection: number[] = [0];
    public sort: SortDescriptor[] = [
        {
            field: 'endDate',
            dir: 'desc'
        }
    ];
    public selectableSettings: SelectableSettings = {
        mode: 'multiple',
        drag: false,
        enabled: true
        
    };

    private taskQueryParam = {
        type: 'team',
        startDate: '01.01.2019 00:00',
        endDate: '01.01.2050 00:00',
        status: 'offen',
        sageKey: '',
        parentId: '13,14,26,33,37,70',
        subFlag: '406',
        userId: '0'
    }
  
    constructor(private gridService: TaskService, public commonShare: CommonService) {
        this.view = gridService;
        this.gridService.query(this.state, this.taskQueryParam);
        this.view.subscribe(res => {
            if(res?.data?.length) {
                let currentTaskIndex = this.defaultSelection[0];
                if(!res.data[currentTaskIndex]) {
                    currentTaskIndex = 0;
                }
                this.commonShare.onTaskRowClick({ dataItem: res.data[currentTaskIndex] });
            }
        })
    }

    ngOnInit(): void {
        this.commonShare.sidebarCommonFilter.subscribe(response => {
            this.taskQueryParam.status = response.key;
            this.taskQueryParam.startDate = response.startDate;
            this.taskQueryParam.endDate = response.endDate;
            this.gridService.query(this.state, this.taskQueryParam);
        })

        this.commonShare.refreshTaskGrid.subscribe(() => this.onReloadClick());

        // setTimeout(() => {
        //     console.log(document.querySelector('tbody').firstElementChild);
        //     // document.querySelector('tbody').firstElementChild.click();
        // }, 5000)
    }

    ngAfterViewInit(){
          if(this.taskGrid) {

          }
        //   const task = this.taskGrid.data.data;
        //   if(task.length) {
        //     this.commonShare.onTaskRowClick(task.id);
        //   }
      }

    public dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
        console.log(this.state);
        this.gridService.query(state, this.taskQueryParam);
    }

    public docViewer(docs):void {
        console.log(docs);
    }

    public onRowSelect(event: CellClickEvent) {
        if ( this.commonShare.isTaskDataChanged ) {
            console.log('changed');
        }
        this.commonShare.onTaskRowClick(event);
    }

    public onRowSelect1(event) {
        console.log(event);
    }

    public rowCallback(context: RowClassArgs) {
        const isEven = context.index % 2 == 0;
        return {
            even: isEven,
            odd: !isEven
        };
    }

    onReloadClick(){
        this.gridService.query(this.state, this.taskQueryParam);
    }

}
