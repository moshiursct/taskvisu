import { Component, OnInit, Input, AfterContentInit } from '@angular/core';
import { DropDownFilterSettings } from '@progress/kendo-angular-dropdowns';
import { groupBy, GroupResult } from '@progress/kendo-data-query';
import { Module } from 'src/app/models/module.model';
import { Sprint } from 'src/app/models/sprint.model';
import { TaskType } from 'src/app/models/task-type.model';
import { Task } from './../../../../models/task.model';
import { CommonService } from 'src/app/services/shared/common.service';
import { User } from 'src/app/models/user.model';
import { FormControl, FormGroup } from '@angular/forms';
import { Category } from 'src/app/models/category.model';

@Component({
	selector: 'app-task-form',
	templateUrl: './task-form.component.html',
	styleUrls: ['./task-form.component.css']
})
export class TaskFormComponent implements OnInit, AfterContentInit {
	@Input() isNew: boolean;
	@Input() taskForm: FormGroup;
	public filterSettings: DropDownFilterSettings = {
		caseSensitive: false,
		operator: 'contains'
	};

	public userList: Array<any> = [];
	public taskTypes: Array<TaskType> = [];
	public modules: Array<Module> = [];
	public clients = [];
	public sprints: Array<Sprint> = [];
	public categories: Array<Category> = [];
	public groupedTeams: Array<any> = [];
	public groupedData: GroupResult[];
	public datePlaceHolder: Object = { 'year': '', 'month': '', 'day': '' };
	public inputPermissions: any = {};

	public task: Task = {
		id: null,
		typeId: null,
		typeName: null,
		clientId: null,
		sprintId: null,
		moduleId: null,
		categoryId: null,
		responsibleNr: null,
		subResponsibles: null,
		personNr: null,
		contactPersons: null,
		startDate: null,
		endDate: null,
		priorityColor: null,
		teamId: null,
		regelMeetingId: null,
		projectId: null,
		meetingId: null,
		isRead: null,
		progress: null,
		title: null,
		details: null,
		closedDate: null,
		estTime: null,
		parentName: null,
		consumendTime: null,
		attachments: null,
		partsNr: null,
		machineNr: null,
		clientNr: null,
		toolsNr: null,
		supplierNr: null,
		updatedBy: null,
		createdBy: null,
		isArchived: null,
		updatedAt: null,
		createdAt: null
	};
	public priorityColorList: Array<{ colorCode: string, colorName: string, colorClass: string }> = [
		{ "colorCode": "#0069d9", "colorName": "Blue", "colorClass": "primary" },
		{ "colorCode": "#449d44", "colorName": "Green", "colorClass": "success" },
		{ "colorCode": "#ec971f", "colorName": "Yellow", "colorClass": "warning" },
		{ "colorCode": "#d9534f", "colorName": "Red", "colorClass": "danger" }
	];
	constructor(public commonShare: CommonService) {
		this.setDatePlaceHolder();
	}

	ngOnInit(): void {
		console.log(this.commonShare.teamList);
		this.userList = [...this.commonShare.userList];
		this.taskTypes = [...this.commonShare.taskTypeList];
		this.clients = [...this.commonShare.clientList];
		this.modules = [...this.commonShare.moduleList];
		this.sprints = [...this.commonShare.sprintList];
		this.categories = [...this.commonShare.categoryList];
		this.formatTeams();

		this.taskForm.addControl("title", new FormControl(""));
		this.taskForm.addControl("details", new FormControl(""));
		this.taskForm.addControl("typeId", new FormControl(0));
		this.taskForm.addControl("startDate", new FormControl(new Date()));
		this.taskForm.addControl("personNr", new FormControl(0));
		this.taskForm.addControl("contactPersons", new FormControl([]));
		this.taskForm.addControl("moduleId", new FormControl(0));
		this.taskForm.addControl("categoryId", new FormControl(0));
		this.taskForm.addControl("teamId", new FormControl(0));
		this.taskForm.addControl("endDate", new FormControl(new Date()));
		this.taskForm.addControl("responsibleNr", new FormControl(0));
		this.taskForm.addControl("clientId", new FormControl(0));
		this.taskForm.addControl("sprintId", new FormControl(0));
		this.taskForm.addControl("priorityColor", new FormControl(""));
		this.taskForm.addControl("subResponsibles", new FormControl([]));
		this.taskForm.addControl("progress", new FormControl(0));
		this.taskForm.addControl("estTime", new FormControl(new Date(2020, 2, 10, 0, 0, 0)));
	}

	getDate(type: string): any {
		const today = new Date();
		const year: number = today.getFullYear();
		let month: any = (today.getMonth() + 1);
		let day: any = today.getDate();
		month = month < 10 ? `0${month}` : month;
		day = day < 10 ? `0${day}` : day;
		if(type === '.') return `${year}.${month}.${day}`;	
		else return { year, month, day };
	}

	setDatePlaceHolder(): void {
		const today = new Date();
		const year: number = today.getFullYear();
		let month: any = (today.getMonth() + 1);
		let day: any = today.getDate();
		month = month < 10 ? `0${month}` : month;
		day = day < 10 ? `0${day}` : day;
		this.datePlaceHolder = { year, month, day };
	}

	formatTeams(): void {
		if(this.isNew) {
			this.groupedTeams = [...this.commonShare.teamList];
		} else {
			const tempTeams = [...this.commonShare.teamList];
			tempTeams.map(({ id, name, memberList }) => {
				memberList.map(member_id => {
					const user = this.userList.find((user: User) => user.id == member_id.toString());
					if (user) this.groupedTeams.push({id, name, member: user.name});
				})
			})
			this.groupedData = groupBy(this.groupedTeams, [{ field: "name" }]);
			this.groupedTeams = this.groupedData;
		}
	}

	// public onTypeChange({permissions}): void {
	// 	console.log(permissions);
	// 	if (permissions) {
	// 		this.inputPermissions = permissions;
	// 	} else {
	// 		this.inputPermissions = {};
	// 	}
	// }

	ngAfterContentInit(): void {
		const { typeId } = this.taskForm.value;
		if ( typeId && !this.isNew) this.getInputPermission(typeId);
	}

	ngAfterViewInit(): void {
		this.taskForm.get('typeId').valueChanges.subscribe( typeId => this.getInputPermission(typeId));
	}

	private getInputPermission(typeId: number): void {
		this.inputPermissions = this.commonShare.taskTypeList.find(item => item.id == typeId)?.permissions;
		if( !this.inputPermissions ) this.inputPermissions = {};
	}

}
