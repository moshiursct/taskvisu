import { Component, OnInit, AfterViewInit, Output, EventEmitter, DoCheck, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UploadComponent } from '@progress/kendo-angular-upload';
import { TaskService } from 'src/app/services/http/task.service';
import { CommonService } from 'src/app/services/shared/common.service';
import { NotificationService } from 'src/app/services/shared/notification.service';
import { objectEquals } from './../../../../helpers/helper';

@Component({
	selector: 'app-new-task',
	templateUrl: './new-task.component.html',
	styleUrls: ['./new-task.component.css']
})
export class NewTaskComponent implements OnInit, AfterViewInit, DoCheck {
	@Output() closeTaskWindow = new EventEmitter<any>();
	@Input() isNew: boolean;
	public taskForm: FormGroup;
	public fileCount: number = 0;
	public fileUploader: UploadComponent;
	public isFormDataChanged: boolean = false;
	public taskCopy: any;
	public addTaskErr = [];
	public devStatusList: Array<any> = [];
	public permissions: any;
	constructor(
		private taskService: TaskService,
		private commonShare: CommonService,
		private notificationService: NotificationService
	) {
		this.closeChangeWarning = this.closeChangeWarning.bind(this);
		this.saveChangedData 	= this.saveChangedData.bind(this);
	}

	ngOnInit(): void {
		this.taskForm = new FormGroup({});
		console.log(this.commonShare);
	}
	
	ngAfterViewInit(): void {
		console.log(this.taskForm);
		this.taskForm.valueChanges.subscribe(input => {
			this.permissions = this.commonShare.taskTypeList.find(item => item.id == input?.typeId)?.permissions;
			
		})
		this.taskCopy = { ...this.taskForm.value };
	}

	ngDoCheck(): void {
		if(objectEquals(this.taskCopy, this.taskForm.value)) {
			this.isFormDataChanged = false;
		} else {
			this.isFormDataChanged = true;
		}
	}
	
	submitData(e): void {
		if (this.validateData("add_task")) {
			if (this.fileCount) {
				this.fileUploader.uploadFiles();
			} else {
				this.saveTask();
			}
		} else {
			for (let index = 0; index < this.addTaskErr.length; index++) {
				this.notificationService.showErrorMsg(this.addTaskErr[index]);
			}
			this.closeChangeWarning(this.addTaskErr);
			this.addTaskErr = [];
		}
	}

	uploadCompleteEvent(attachments: Array<any>): void {
		console.log(attachments);
		if (attachments.length) {
			this.taskForm.value.attachments = attachments;
		}
		this.saveTask();
	}

	saveTask(): void {
		const task = { ...this.taskForm.value, partsNr: "", machineNr: "", toolsNr: "", clientNr: "", supplierNr: "", of: 1, currentUser: '101004', requirements: this.devStatusList };
		this.taskService.save(task).subscribe(response => {
			console.log(response);
			if(response) {
				this.notificationService.showSuccessMsg("Task Saved");
				this.closeTaskWindow.emit();
				this.closeChangeWarning([]);
				this.commonShare.onTaskGridRefesh();
			}
		});
	}

	closeWindow(): void {
		if ( this.isFormDataChanged ) this.isChangeWarningModalOpen = true;
		else this.closeTaskWindow.emit();
		console.log(this.isFormDataChanged);
	}

	validateData(type: string): boolean {
		let data = { ...this.taskForm.value };
		if (!data.typeId && type === "add_task") {
			this.addTaskErr = [`Error: Task Type is required !!!`];
			return false;
		}

		let requiredConfig = {};
		let currentTaskType = this.commonShare.taskTypeList.find(item => item.id == data.typeId);

		if (currentTaskType) {
			requiredConfig = { ...currentTaskType.permissions };
		} else {
			requiredConfig = { ...this.commonShare.taskTypeList.find(item => item.id == 1)?.permissions };
		}

		Object.keys(requiredConfig).forEach(property => {
			if (property !== "" && property !== 'tabAttachments' && property !== 'tabNote' && property !== 'tabTimesheet' && property !== 'tabDevStatus' && requiredConfig[property].isMandatory) {
				let value: any = data[property];
				if (value.toString() == ""  || value.toString() == "0") {
					this.addTaskErr.push(`Error: ${requiredConfig[property].name} is required !!!`);
				}
			}
		});

		if (this.addTaskErr.length) return false;
		else return true;
	}

	public isChangeWarningModalOpen: boolean = false;

	saveChangedData(): void {
		this.submitData('');
	}

	closeChangeWarning(errors: Array<any>[]): void {
		if ( !this.isFormDataChanged && !errors.length) this.closeTaskWindow.emit();
		if(!errors) this.closeTaskWindow.emit();
		this.isChangeWarningModalOpen = false;
	}
}
