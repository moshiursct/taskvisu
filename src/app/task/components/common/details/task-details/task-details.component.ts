import { AfterContentInit, AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { TaskService } from 'src/app/services/http/task.service';
import { CommonService } from './../../../../../services/shared/common.service'
import { FormGroup, NgForm } from '@angular/forms';
import { Task } from '../../../../../models/task.model';
import { DropDownFilterSettings } from '@progress/kendo-angular-dropdowns';
import { Module } from 'src/app/models/module.model';
import { CommonHttpService } from 'src/app/services/http/common.service';
import { TaskType } from 'src/app/models/task-type.model';
import { Sprint } from 'src/app/models/sprint.model';
import { Team } from 'src/app/models/team.model';
import { GroupResult, groupBy } from '@progress/kendo-data-query';
@Component({
	selector: 'app-task-details',
	templateUrl: './task-details.component.html',
	styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent implements OnInit, OnChanges, AfterContentInit {

	@Input() event: FormGroup;
	@Output() attachmentUploaded = new EventEmitter();
	public listItems: Array<any>;

  public userList: Array<any> = [];
  public task: Task = {
    id: null,
    typeId: null,
    typeName: null,
    clientId: null,
    sprintId: null,
    moduleId: null,
    categoryId: null,
    responsibleNr: null,
    subResponsibles: null,
    personNr: null,
    contactPersons: null,
    startDate: null,
    endDate: null,
    priorityColor: null,
    teamId: null,
    regelMeetingId: null,
    projectId: null,
    meetingId: null,
    isRead: null,
    progress: null,
    title: null,
    details: null,
    closedDate: null,
    estTime: null,
    parentName: null,
    consumendTime: null,
    attachments: null,
    partsNr: null,
    machineNr: null,
    clientNr: null,
    toolsNr: null,
    supplierNr: null,
    updatedBy: null,
    createdBy: null,
    isArchived: null,
    updatedAt: null,
    createdAt: null
  };

  public priorityColorList: Array<{ colorCode: string, colorName: string, colorClass: string }> = [
    { "colorCode": "#0069d9", "colorName": "Blue", "colorClass": "primary" },
    { "colorCode": "#449d44", "colorName": "Green", "colorClass": "success" },
    { "colorCode": "#ec971f", "colorName": "Yellow", "colorClass": "warning" },
    { "colorCode": "#d9534f", "colorName": "Red", "colorClass": "danger" }
  ];

  public modules: Array<Module> = [];
  public clients;
  public taskTypes: Array<TaskType> = []
  public sprints: Array<Sprint> = []
  public teams: Array<Team> = []
  public loading: boolean = false;
  public teamMemberList: Array<any> = []
  public groupedData: GroupResult[];
  constructor(private commonShare: CommonService, public taskService: TaskService, public commonHttp: CommonHttpService) {
    console.log(`Constructor: ${this.taskService.loading}`);

  }

  ngOnInit(): void {
    this.modules = this.commonShare.moduleList;
    this.clients = this.commonShare.clientList;
    this.getTaskTypeData();
    this.getSprintData();
    this.getTeamData();
    this.commonShare.taskRowClicked.subscribe(response => {
      this.getTaskDetails(response.dataItem.id);
      // console.log(this.commonShare.userList);
      this.listItems = this.commonShare.userList;
      this.userList = this.listItems;
      this.taskService.fileCount = 0;
    }, error => { console.log(error) }, () => {

    });


  }


  getTaskDetails(taskId: number): void {
    this.loading = true;
    this.taskService.taskDetails(taskId).subscribe(response => {
      this.loading = false;
      this.task = { ...this.formatTaskData(response) };
      this.commonShare.currentTask = { ...this.task };
    }, error => {
      console.log(error);
    }, () => {
      console.log("completed")
    })
  }

  getTaskTypeData() {
    this.commonHttp.getTaskType().subscribe(taskTypes => {
      this.taskTypes = taskTypes;
    }, error => {
      console.log(error);
    },
      () => {
        console.log("Completed")
      }
    )
  }
  getSprintData() {
    this.commonHttp.getSprints().subscribe(sprints => {
      this.sprints = sprints;
    })
  }
  getTeamData() {
    this.commonHttp.getTeams().subscribe(teams => {
      this.teams = teams;
      this.teams.map(team => {
        team.memberList.map(member => {
          this.userList.map((user) => {
            if (member === user.id) {
              this.teamMemberList.push({ id: team.id, name: team.name, member: user.name })
            }
          })
        })
      })
      this.groupedData = groupBy(this.teamMemberList, [{ field: "name" }])
    })
  }

  formatTaskData(data): Task {
    return {
      id: data.id,
      typeId: data.type_id,
      typeName: data.task_type,
      clientId: data.client_id,
      sprintId: data.sprint_id,
      moduleId: data.module_id,
      categoryId: data.category_id,
      responsibleNr: data.responsible_person_nr,
      subResponsibles: data.sub_responsibles,
      personNr: data.person_nr,
      contactPersons: data.contact_persons,
      startDate: this.formatDate(data.start_date),
      endDate: this.formatDate(data.end_date),
      priorityColor: data.probability_color,
      teamId: data.team_id,
      regelMeetingId: data.regelmeeting_id,
      projectId: data.project_id,
      meetingId: data.meeting_id,
      isRead: data.is_read,
      progress: parseInt(data.erledigt),
      title: data.problem,
      details: data.task_status,
      closedDate: data.closed_date,
      estTime: this.formatTime(data.est_time),
      parentName: data.parent_name,
      consumendTime: data.total_elapsed_time,
      attachments: data.all_docs.map((item: any) => {
        return {
          "id": item.doc_id,
          "extension": item.doc_extension,
          "name": item.doc_original_name,
          "path": item.doc_path,
          "size": item.doc_size,
          "modifiedName": item.doc_file_name
        }
      }),
      partsNr: data.teile_nr,
      machineNr: data.maschine_nr,
      clientNr: data.kunden_nr,
      toolsNr: data.wkz_nr,
      supplierNr: data.lieferant_nr,
      updatedBy: data.updated_by,
      createdBy: data.insert_person_nr,
      isArchived: data.is_archived,
      updatedAt: data.updated_at,
      createdAt: data.cerated_at
    };
  }

  public filterSettings: DropDownFilterSettings = {
    caseSensitive: false,
    operator: 'contains'
  };


	updateTaskData(form: NgForm) {
		//this.attachmentUploaded.emit(null)
		this.taskService.taskData = this.task;
		if (this.taskService.fileCount !== 0) {
			this.taskService.upload();
		} else {
			this.taskService.saveTaskData();
		}
	}
	private formatDate(date: string): Date {
		const splitDate = date.split('.');
		return new Date(parseInt(splitDate[2]), parseInt(splitDate[1]), parseInt(splitDate[0]));
	}
	private formatTime(date: string): Date {
		const splitDate = date.split(':');
		return new Date(0, 0, 0, parseInt(splitDate[0]), parseInt(splitDate[1]));
	}

	ngOnChanges() {
		// console.log(`On changes: ${this.loading}`)
	}
	ngAfterContentInit() {
		// console.log(`After Content init: ${this.loading}`)
	}

}
