import { Component, DoCheck, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { SelectEvent, TabStripComponent } from '@progress/kendo-angular-layout';
import { UploadComponent } from '@progress/kendo-angular-upload';
import { objectEquals } from 'src/app/helpers/helper';
import { Task } from 'src/app/models/task.model';
import { TaskService } from 'src/app/services/http/task.service';
import { CommonService } from 'src/app/services/shared/common.service';
import { CommonHttpService  } from 'src/app/services/http/common.service';
import { NotificationService } from 'src/app/services/shared/notification.service';
import { File } from './../../../../models/file.model';
import { ApiUrls } from './../../../../../environments/environment';
import { Observable } from 'rxjs';

@Component({
	selector: 'app-details',
	templateUrl: './details.component.html',
	styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit,DoCheck {
	@ViewChild('tabStrip') public tabStrip: TabStripComponent;
	public taskForm: FormGroup;
	selectedTab: string = 'Details';
	attachments: Array<File> = [];
	deletedAttachments: any = [];
	public taskRowSelected: boolean = false;
	public loading: boolean = false;
	public inputPermissions: any = {};
	public fileUploader: UploadComponent;
	public fileCount: number = 0;
	private taskCopy: Task;
	public isChangeWarningModalOpen: boolean = false;
	public isFirstLoad: boolean = false;
	public isTaskDeleteWarningOpen: boolean = false;

	constructor(private commonShare: CommonService, private commonHttp: CommonHttpService, public taskService: TaskService, public notificationService: NotificationService, private http: HttpClient) {
		this.closeTaskDeleteWarning = this.closeTaskDeleteWarning.bind(this);
		this.confirmDelete = this.confirmDelete.bind(this);
	}

	public taskTimerId = null;
	public taskStartWindow = false;
	public taskId = null;
	public isNew = true;
	public timerData = null;
	public timerLoading = true;

	windowTop = 250
	windowLeft = -300
	currentAttachments: Array<File> = [];
	public currentTime = this.getCurrentTime("-");
	time: number = 0;
	display ="00:00:00";
	interval;



	ngOnInit(): void {
		this.taskForm = new FormGroup({consumedTime: new FormControl()});
		this.commonShare.taskRowClicked.subscribe((response: any) => {
			this.taskRowSelected = true;
			Promise.resolve(null).then(() => this.tabStrip.selectTab(0));
			this.getTaskDetails(response.dataItem.id);
			this.taskId = response.dataItem.id;
			// this.currentAttachments = response.dataItem.all_docs.map((item: any) => {
			// 	return {
			// 		"id": item.doc_id,
			// 		"extension": item.doc_extension,
			// 		"name": item.doc_original_name,
			// 		"path": item.doc_path,
			// 		"size": item.doc_size,
			// 		"modifiedName": item.doc_file_name
			// 	}
			// });
		});
		Promise.resolve(null).then(() => {
			this.tabStrip.tablist.nativeElement.lastElementChild.className = this.tabStrip.tablist.nativeElement.lastElementChild.className.replace('k-state-disabled', "task-action-items");
			console.log('tab', this.tabStrip.tablist.nativeElement.lastElementChild.className);
		})
		console.log(this.display)
		this.commonHttp.getUserTimerData().subscribe((data:any) => {
			console.log(data);
			if (data) {
				if (data.task_id) {
					this.currentTime = data.start_time;
					this.taskTimerId = data.task_id;
					console.log(this.taskTimerId);
					console.log(this.taskId);
					
				}
			} 
			this.timerData =  data;	
		 	this.timer();		
		},
		(error) => {
			console.log(error);
		},
		() => {
			this.timerLoading = false;
		}
		);
	}

	public onClickStart() {
		this.taskStartWindow = true;
		console.log(this.taskStartWindow);

	}
	public close() {
		this.taskStartWindow = false;
	}

	ngAfterViewInit(): void {
		this.taskForm.get('typeId').valueChanges.subscribe(typeId => {
			this.inputPermissions = this.commonShare.taskTypeList.find(item => item.id == typeId)?.permissions;
			if( !this.inputPermissions ) this.inputPermissions = {};
		})
	}

	getTaskDetails(taskId: number): void {
		this.loading = true;
		this.taskService.taskDetails(taskId).subscribe(response => {
			const task = { ...this.formatTaskData(response) };
			this.commonShare.currentTask = { ...task };
			console.log(task);
			this.taskForm.patchValue({...task});
			// this.inputPermissions = this.commonShare.taskTypeList.find(item => item.id == task.typeId)?.permissions;
			this.taskCopy = {...this.taskForm.value}
		}, error => {
			console.log(error);
		}, () => {
			this.loading = false;
			console.log("completed")
		})
	}

	formatTaskData(data): Task {
		return {
			...data,
			startDate: new Date(data.startDate),
			endDate: new Date(data.endDate),
			progress: parseInt(data.progress),
			estTime: this.formatTime(data.estTime),
			consumedTime: new Date(new Date(2020, 2, 10, 0, 0, 0))
		};
	}

	private formatTime(date: string): Date {
		const splitDate = date.split(':');
		return new Date(0, 0, 0, parseInt(splitDate[0]), parseInt(splitDate[1]));
	}

	onTabSelect(e: SelectEvent): void {
		this.selectedTab = e.title;
	}

	submitData(): void {
		console.log(this.taskForm.value, this.commonShare.isTaskDataChanged);
		if(this.deletedAttachments.length) {
			this.commonHttp.deleteAttachments(this.deletedAttachments).subscribe(
				response => console.log(response),
				err => console.log(err),
				() => this.submitFinal()
			);
		} else this.submitFinal();
		
	}

	submitFinal(): void {
		if( this.fileCount ) {
			this.fileUploader.uploadFiles();
		} else{
			this.taskForm.value.attachments = [];
			this.save();
		}
	}

	public attachmentUploadCompleteEvent(attachments: Array<File>): void {
		console.log(attachments);
		if (attachments.length) {
			this.taskForm.value.attachments = attachments;
		}
		this.save();
	}

	public attachmentDeleteEvent(attachment: File): void {
		console.log(attachment);
		this.deletedAttachments.push(attachment);
	}
	
	ngDoCheck(): void {
		if(objectEquals(this.taskCopy, this.taskForm.value) && !this.fileCount) {
			this.commonShare.isTaskDataChanged = false;
		} else {
			this.commonShare.isTaskDataChanged = true;
		}
	}

	saveChangedData(): void {
		this.submitData();
	}

	closeChangeWarning(errors: Array<any>[]): void {
		this.isChangeWarningModalOpen = false;
	}
	save(): void {
		const task = { ...this.taskForm.value, id: this.commonShare.currentTask.id, partsNr: "", machineNr: "", toolsNr: "", clientNr: "", supplierNr: "", of: 1, currentUser: '101004', requirements: [] };
		this.taskService.save(task).subscribe(response => {
			console.log(response);
			if(response) {
				this.commonShare.onTaskGridRefesh();
				this.notificationService.showSuccessMsg("Task Saved");
			}
		});
		console.log(task);
	}

	closeTaskDeleteWarning(): void {
		this.isTaskDeleteWarningOpen = false;
	}
	
	delete(): void {
		this.isTaskDeleteWarningOpen = true;
	}
	
	confirmDelete(): void {
		const { id } = this.commonShare.currentTask;
		
		this.taskService.delete(id).subscribe(
			(reponse)=> console.log(reponse),
			(err)=> console.log(err),
			()=> {
				this.isTaskDeleteWarningOpen = false,
				this.taskForm.reset();
				this.commonShare.onTaskGridRefesh();
				this.notificationService.showSuccessMsg("Task Deleted");
			}
		)
	}

	startTimer($event) {
		this.currentTime = this.getCurrentTime("-");
		console.log($event)
		let timeData = {
			is_new: true,
			current_time:this.currentTime,
			user_id:'406',
			task_id:this.taskId,
			status_id:$event.statusId,
			note:$event.note
		}
		console.log(timeData);
		
		this.http.post(`${ApiUrls.taskVisu.time}save_timer_data`,timeData).subscribe((response:any) => {
			console.log(response);
			this.taskTimerId = response.task_id;
			this.timer();
		})
		
		
	}

	stopTimer() {

		let timeData = {
			is_new: false,
			current_time:this.currentTime,
			user_id:'406',
			task_id:this.taskId,
		}
		this.http.post(`${ApiUrls.taskVisu.time}save_timer_data`,timeData).subscribe((response:any) => {
			console.log(response);
			this.taskTimerId = response.task_id
			//this.timer();
		})
	}

	private timer(): void {
		this.interval = setInterval(() => {
			this.timerUpdate()
			this.display = this.timerUpdate()
		}, 1000);
	}

	pauseTimer(): void {
		this.currentTime= null
		clearInterval(this.interval);
		this.display = null
	}

	getCurrentTime(type: string): string {
        const today = new Date();
        if (type == '.') {
            const date = today.getDate() + '.' + (today.getMonth() + 1) + '.' + today.getFullYear();
            const time = today.getHours() + ":" + today.getMinutes();
            return date + ' ' + time;
        }
        const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        return date + ' ' + time;
        
	}
	
	timerUpdate() {
        /* Get task starting time and cenvert in timestamp */
        const t_prev = new Date(this.currentTime).getTime();
        /* Get current time and cenvert in timestamp */
        const t_curr = new Date(Date.now()).getTime();

        let time_diff:any = new Date(t_curr - t_prev);
        time_diff = time_diff.getTime() / 1000;

        /* Get Hour, Minute, Seconds */
        let hours:any = Math.floor(time_diff / 3600);
        let minutes:any = time_diff % 3600;
        let seconds:any = Math.floor(minutes % 60);
		minutes = Math.floor(minutes / 60);
		
		if (hours < 10) { hours = '0' + hours; }
		if (minutes < 10) { minutes = '0' + minutes; }
		if (seconds < 10) { seconds = '0' + seconds; }
		return hours + ':' + minutes + ':' + seconds;
        // $scope.current_task_time.h = h < 10 ? ('0' + h) : h;
        // $scope.current_task_time.m = m < 10 ? ('0' + m) : m;
        // $scope.current_task_time.s = s < 10 ? ('0' + s) : s;

        // $scope.$applyAsync();
    }
}
