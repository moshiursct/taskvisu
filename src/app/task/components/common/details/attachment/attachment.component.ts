import { Component, Input, Output, OnInit, AfterViewInit, ViewChild, EventEmitter } from '@angular/core';
import { UploadComponent, SelectEvent, RemoveEvent, SuccessEvent, UploadProgressEvent } from '@progress/kendo-angular-upload';
import { ApiUrls } from './../../../../../../environments/environment';
import { File } from './../../../../../models/file.model';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
	selector: 'app-attachment',
	templateUrl: './attachment.component.html',
	styleUrls: ['./attachment.component.css'],
	providers: []
})
export class AttachmentComponent implements OnInit, AfterViewInit {
	@ViewChild('appAttachment') public appAttachment: UploadComponent;

	@Input() currentAttachments: Array<File>;
	@Input() parentForm: FormGroup;

	@Output() completeEvent = new EventEmitter<any>();
	@Output() deleteEvent = new EventEmitter<any>();
	@Output() fileCounter = new EventEmitter<any>();
	@Output() uploader = new EventEmitter<any>();
	
	public isModalOpen: boolean = false;
	public newAttachments: Array<File> = [];
	public deletedAttachment: any = {};
	public fileSelectedForDeletion: any;
	public fileCount: number = 0;
	public uploadPercentage: Array<string> = [];

	public uploadSaveUrl = `${ApiUrls.taskVisu.task_new}upload_attachment`;
	public uploadRemoveUrl = `${ApiUrls.taskVisu.task_new}demo_remove`;

	constructor() {
		this.cancelDelete = this.cancelDelete.bind(this);
		this.confirmDelete = this.confirmDelete.bind(this);
	}

	ngOnInit(): void {
		if (!this.currentAttachments) {
			this.currentAttachments = [];
		}
		this.parentForm.addControl("attachments", new FormControl(this.currentAttachments));
	}

	ngAfterViewInit(): void {
		this.uploader.emit(this.appAttachment);
		document.querySelector(".k-upload .k-dropzone .k-upload-button input").nextElementSibling.innerHTML = `
			<span class="select-placeholder-img"></span>
			<span class="select-placeholder-label">Drag a file here or click to upload</span>
		`;
	}

	public clearEventHandler(): void {
		this.fileCount = 0;
	}

	public completeEventHandler() {
		this.fileCount = 0;
		this.fileCounter.emit(this.fileCount);
		this.completeEvent.emit(this.newAttachments);
		this.newAttachments = [];
	}

	public successEventHandler(e: SuccessEvent) {
		if (e.operation == "upload") {
			this.newAttachments.push({
				name: e.response.body.name,
				extension: e.response.body.extension.toLowerCase(),
				modifiedName: e.response.body.modifiedName,
				path: e.response.body.path,
				size: e.response.body.size
			});
		}
	}

	public removeEventHandler(e: RemoveEvent): void { }

	public selectEventHandler(e: SelectEvent): void {
		this.fileCount += (e.files.length);
		this.fileCounter.emit(this.fileCount);
	}

	public uploadProgressEventHandler(e: UploadProgressEvent) {
		this.uploadPercentage[e.files[0].uid] = e.percentComplete;
	}

	public remove(upload: UploadComponent, uid: string, files: File) {
		if (!files.id) {
			upload.removeFilesByUid(uid);
			
			if (this.fileCount > 0) {
				this.fileCount--;
			}
		} else {
			this.fileSelectedForDeletion = { ...files };
			this.isModalOpen = true;
		}
	}

	public confirmDelete() {
		const { id, modifiedName, path, uid } = this.fileSelectedForDeletion;

		this.deletedAttachment = { id, modifiedName, path };
		this.appAttachment.removeFilesByUid(uid);

		this.deleteEvent.emit(this.deletedAttachment);
		
		this.isModalOpen = false;

		console.log(this.deletedAttachment);
	}

	public cancelDelete() {
		this.isModalOpen = false;
	}

	public addExtensionClass({ extension }) {
		switch (extension.toLowerCase()) {
			case '.jpg':
			case '.jpeg':
				return "jpg-file";
			case '.img':
			case '.gif':
				return "img-file";
			case '.png':
				return "png-file";
			case '.doc':
			case '.docx':
				return "doc-file";
			case '.xls':
			case '.xlsx':
				return "xls-file";
			case '.pdf':
				return "pdf-file";
			case '.ppt':
			case '.pptx':
				return "ppt-file";
			default:
				return "default-file";
		}
	}

	public convertByteToKB(size: number) {
		let sizeInKB = Math.ceil(size / 1024);
		return sizeInKB + "Kb";
	}

	public uploadBtnClick(uploadElement: any): void {
		
	}
}
