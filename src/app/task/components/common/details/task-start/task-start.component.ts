import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { interval } from 'rxjs';
import { map, min } from 'rxjs/operators';
import { TaskStatus } from 'src/app/models/task-status.model';
import { CommonService } from 'src/app/services/shared/common.service';

@Component({
  selector: 'app-task-start',
  templateUrl: './task-start.component.html',
  styleUrls: ['./task-start.component.css']
})
export class TaskStartComponent implements OnInit {

  public taskStatusList: Array<TaskStatus> = [];
  public statusId: number;
  public note:string = '';
  
  @Output() windowCloseEvent = new EventEmitter();
  @Output() timerEvent = new EventEmitter();

  constructor(private commonShare: CommonService) { }

  ngOnInit(): void {
    this.taskStatusList = this.commonShare.taskStatusList;
  }
  closeTaskStatus() {
    this.windowCloseEvent.emit(null);
  }
  selectStatusHandler(id) {
    this.statusId = id;
  }
  statusSubmitHandler() {
    let statusData = {
      statusId: this.statusId,
      note: this.note
    }
    this.timerEvent.emit(statusData)
    this.windowCloseEvent.emit(null);
  }



}
