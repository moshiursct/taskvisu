import { Component, OnInit } from '@angular/core';
import { NoteService } from 'src/app/services/http/note.service';
import { CommonService } from 'src/app/services/shared/common.service';
import { NotificationService } from 'src/app/services/shared/notification.service';
import { Note } from './../../../../../models/note.model';

@Component({
  	selector: 'app-note',
  	templateUrl: './note.component.html',
  	styleUrls: ['./note.component.css']
})

export class NoteComponent implements OnInit {
  	noteList: Note[] = [];
	  
	currentNoteId: number;
	currentNote: string;
	taskId: number;
	userId: string = "101006";
	addNoteData: Note;

  	constructor(
		private commonShare: CommonService, 
		private noteService: NoteService, 
		private notificationService: NotificationService
	) { }

	ngOnInit(): void {
		this.taskId = this.commonShare.currentTask.id;
		console.log(this.commonShare.oldTaskId, this.taskId, this.noteList);
		if(this.commonShare.oldTaskId == this.taskId) {
			this.noteList = [...this.commonShare.currentTaskNoteList];
			if (this.noteList.length) {
				this.selectNote(this.noteList[0]);
			}
		} else {
			this.commonShare.oldTaskId = this.taskId;
			this.noteService.getList(this.taskId).subscribe(notes => {
				this.noteList = notes;
				this.commonShare.currentTaskNoteList = [...notes];
				if (this.noteList.length) {
					this.selectNote(this.noteList[0]);
				}
			});
		}
		
	}

	selectNote(note: Note): void {
		this.currentNoteId = note.id;
		this.currentNote = unescape(note.note);
	}

	newNote(): void {
		this.currentNoteId = -1;
		this.currentNote = "";
	}
	
	saveNote(): void {
		if (!this.currentNote) {
			this.notificationService.showErrorMsg("Note details can't be empty.");
			return;
		}

		this.addNoteData = {
			"id": this.currentNoteId,
			"task_id": this.taskId,
			"user_id": this.userId,
			"note": encodeURIComponent(this.currentNote),
		};

		this.noteService.save(this.addNoteData).subscribe((note: Note) => {
			if (note && this.currentNoteId == -1) {
				this.notificationService.showSuccessMsg("Note Added Successfully.");
				this.noteList.unshift(note);
				this.selectNote(this.noteList[0]);
			} else if (this.currentNoteId > -1) {
				this.notificationService.showSuccessMsg("Note Updated Successfully.");
				
				const updatedIndex = this.noteList.findIndex(item => item.id == note.id);

                if (updatedIndex > -1) {
                    this.noteList[updatedIndex] = { ...note };
				}
			}

			this.commonShare.oldTaskId = 0;

		});
	}
}
