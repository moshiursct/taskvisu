import { Component, OnInit, AfterViewChecked, AfterViewInit } from '@angular/core';

@Component({
	selector: 'app-grid-container',
	templateUrl: './grid-container.component.html',
	styleUrls: ['./grid-container.component.css']
})
export class GridContainerComponent implements OnInit, AfterViewChecked, AfterViewInit {
	public isWindowOpened: boolean = false;
	public windowTop: number = 50;
	public isFormDataChanged: boolean = false;
	constructor() {

	}

	ngOnInit(): void {
	}
	ngAfterViewChecked(): void {
		// if (this.taskForm.touched) {
		// 	console.log(this.taskForm);
		// }
	}

	ngAfterViewInit(): void {
		// console.log(this.taskForm);
	}

	addNewTask(): void {
		this.isWindowOpened = true;
	}

	closeWindow(): void {
		this.isWindowOpened = false;
	}

	public closeTaskWindow(): void {
		this.closeWindow();
	}

}
