import { AfterContentChecked, AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { GroupDescriptor, DataResult, process } from "@progress/kendo-data-query";
import { TaskService } from 'src/app/services/http/task.service';
import { CommonService } from "../../../../services/shared/common.service";
import { getCurrentTime } from "../../../../helpers/helper";

@Component({
	selector: 'app-timesheet',
	templateUrl: './timesheet.component.html',
	styleUrls: ['./timesheet.component.css']
})
export class TimesheetComponent implements OnInit {

	constructor(
		private taskService: TaskService,
		private commonService: CommonService) { }
	@ViewChild('timesheetGrid') private timesheetGrid;

	public groups: GroupDescriptor[] = [{ field: 'user_name' }];
	public gridView: DataResult;
	timesheetData: any[] = [];

	taskId: number;
	oldTaskid: number = 0;

	ngOnInit(): void {
		// Call All TimesheetData for task
		this.taskId = this.commonService.currentTask.id;
		// console.log(getCurrentTime('-'));
		// console.log(this.commonService.currentTimeHyphen);
		if (this.oldTaskid != this.taskId) {
			this.oldTaskid = this.taskId;
			this.taskService.getTimeSheetData(this.taskId, getCurrentTime('-')).subscribe((data: any) => {
				this.timesheetData = data;
				this.loadTimesheetData();
			})
		}

	}

	// Load all timesheet data
	loadTimesheetData(): void {
		this.gridView = process(this.timesheetData, { group: this.groups });
		this.gridView.data.forEach((_, index) => {
			this.timesheetGrid.collapseGroup(String(index));
		})
	}

	getTotalTime(index): string {
		// console.log(this.timesheetGrid.data.data[index]);
		this.timesheetGrid.data.data[index].items.map(data => {
			console.log(data.elapsed_time);
		})
		return "";
	}

}
