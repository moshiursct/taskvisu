import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'app-warning-modal',
	templateUrl: './warning-modal.component.html',
	styleUrls: ['./warning-modal.component.css']
})
export class WarningModalComponent implements OnInit {
	@Input() yesBtnTitle: string;
	@Input() noBtnTitle: string;
	@Input() messageText: string;

	@Input() onYesBtnClicked: any;
	@Input() onNoBtnClicked: any;
	@Input() onCloseBtnClicked: any;

	@Input() modalOpened: boolean;

	constructor() { }

	ngOnInit(): void {
		this.modalOpened = this.modalOpened != undefined ? this.modalOpened : true;

		this.yesBtnTitle = this.yesBtnTitle ? this.yesBtnTitle : "Yes";
		this.noBtnTitle = this.noBtnTitle ? this.noBtnTitle : "No";
		this.messageText = this.messageText ? this.messageText : "Custom Message";

		this.onYesBtnClicked = this.onYesBtnClicked ? this.onYesBtnClicked : () => {
			alert("Yes button clicked!");
		};

		this.onNoBtnClicked = this.onNoBtnClicked ? this.onNoBtnClicked : () => {
			alert("No button clicked!");
		};

		this.onCloseBtnClicked = this.onCloseBtnClicked ? this.onCloseBtnClicked : () => {
			this.modalOpened = false;
		};
	}
}
