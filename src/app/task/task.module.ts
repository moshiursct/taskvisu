// angular dependencies
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from '@angular/forms';

// custom modules
import { TaskRoutingModule } from './task-routing.module';

// kendo dependencies
import { GridModule } from "@progress/kendo-angular-grid";
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { EditorModule } from '@progress/kendo-angular-editor';
import { ProgressBarModule } from '@progress/kendo-angular-progressbar';
import { TabStripComponent, TabStripTabComponent } from '@progress/kendo-angular-layout';
import { UploadModule } from '@progress/kendo-angular-upload';
import { WindowModule } from "@progress/kendo-angular-dialog";
import { DialogsModule } from '@progress/kendo-angular-dialog';
import { LabelModule } from '@progress/kendo-angular-label'

// components
import { MyTaskComponent } from './components/my-task/my-task.component';
import { TeamTaskComponent } from './components/team-task/team-task.component';
import { AssignedTaskComponent } from './components/assigned-task/assigned-task.component';
import { MeetingComponent } from './components/meeting/meeting.component';
import { RegelMeetingComponent } from './components/regel-meeting/regel-meeting.component';
import { ProjectComponent } from './components/project/project.component';
import { TaskGridComponent } from './components/common/task-grid/task-grid.component';
import { TaskDetailsComponent } from './components/common/details/task-details/task-details.component';
import { DetailsComponent } from './components/common/details/details.component';
import { TimesheetComponent } from './components/common/timesheet/timesheet.component';
import { NoteComponent } from './components/common/details/note/note.component';
import { AttachmentComponent } from './components/common/details/attachment/attachment.component';

// pipes
import { DateFormatPipe, TimeFormatPipe } from '../pipes/date-time-format.pipe';
import { UserNameFormatPipe } from '../pipes/user-name-format.pipe';
import { DevStatusComponent } from './components/common/dev-status/dev-status.component';
import { DevStatusFormComponent } from './components/common/dev-status/dev-status-form/dev-status-form.component';
import { WarningModalComponent } from './components/common/warning-modal/warning-modal.component';
import { GridContainerComponent } from './components/common/grid-container/grid-container.component';
import { TaskFormComponent } from './components/common/task-form/task-form.component';
import { NewTaskComponent } from './components/common/new-task/new-task.component';
import { ProjectFormComponent } from './components/project/project-form/project-form.component';
import { ProjectDetailsFormComponent } from './components/project/project-form/project-details-form/project-details-form.component';
import { TaskStartComponent } from './components/common/details/task-start/task-start.component';
import { GanttComponent } from './components/project/project-form/gantt/gantt.component';
import { GanttFormComponent } from './components/project/project-form/gantt/gantt-form/gantt-form.component';
import { GanttDetailsFormComponent } from './components/project/project-form/gantt/gantt-form/gantt-details-form/gantt-details-form.component';

// directives

// import { TabContentLoadOnDemandDirective } from './../directives/tabstrip-lazy-load.directives';

@NgModule({
  declarations: [
    MyTaskComponent,
    TeamTaskComponent,
    AssignedTaskComponent,
    MeetingComponent,
    RegelMeetingComponent,
    ProjectComponent,
    TaskGridComponent,
    TaskDetailsComponent,
    DetailsComponent,
    TimesheetComponent,
    NoteComponent,
    AttachmentComponent,
    DateFormatPipe,
    TimeFormatPipe,
    UserNameFormatPipe,
    DevStatusComponent,
    DevStatusFormComponent,
    WarningModalComponent,
    GridContainerComponent,
    TaskFormComponent,
    NewTaskComponent,
    ProjectFormComponent,
    ProjectDetailsFormComponent,
    TaskStartComponent,
    GanttComponent,
    GanttFormComponent,
    GanttDetailsFormComponent
  ],
  exports:[
    MyTaskComponent,
    TeamTaskComponent,
    AssignedTaskComponent,
    MeetingComponent,
    RegelMeetingComponent,
    ProjectComponent,
    TaskDetailsComponent
  ],
  imports: [
    TaskRoutingModule,
    CommonModule,
    HttpClientModule,
    GridModule,
    DateInputsModule,
    LayoutModule,
    DropDownsModule,
    InputsModule,
    EditorModule,
    ProgressBarModule,
    UploadModule,
    WindowModule,
    DialogsModule,
    LabelModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    TabStripComponent
  ],
  providers: [
    TabStripTabComponent
  ],
})
export class TaskModule { }
