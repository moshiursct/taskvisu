import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyTaskComponent } from './components/my-task/my-task.component';
import { TeamTaskComponent } from './components/team-task/team-task.component';
import { AssignedTaskComponent } from './components/assigned-task/assigned-task.component';
import { MeetingComponent } from './components/meeting/meeting.component';
import { RegelMeetingComponent } from './components/regel-meeting/regel-meeting.component';
import { ProjectComponent } from './components/project/project.component';

const routes: Routes = [
	{ path: '', redirectTo: 'my_task', pathMatch: 'full' },
	{ path: "my_task", component: MyTaskComponent, data: { title: 'My Task' } },
	{ path: "team_task", component: TeamTaskComponent, data: { title: 'Team Task' } },
	{ path: "assigned_task", component: AssignedTaskComponent, data: { title: 'Assigned Task' } },
	{ path: "meeting", component: MeetingComponent, data: { title: 'Meeting' } },
	{ path: "project", component: ProjectComponent, data: { title: 'Project' } },
	{ path: "regel_meeting", component: RegelMeetingComponent, data: { title: 'Regular Meeting' } }
];

@NgModule({
	imports: [RouterModule.forChild(routes)]
})
export class TaskRoutingModule { }